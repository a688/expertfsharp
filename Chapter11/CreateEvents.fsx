﻿module CreateEvents

open System
open System.Windows.Forms

type RandomTicker(approxInterval) =
    let timer = new Timer()
    let rnd = new System.Random(99)
    let tickEvent = new Event<int>()

    let chooseInterval() : int = approxInterval + approxInterval / 4 - rnd.Next(approxInterval / 2)

    do timer.Interval <- chooseInterval()

    do timer.Tick.Add(
        fun args ->
            let interval = chooseInterval()
            tickEvent.Trigger interval;
            timer.Interval <- interval
        )

    member x.RandomTick = tickEvent.Publish
    member x.Start() = timer.Start()
    member x.Stop() = timer.Stop()
    interface IDisposable with
        member x.Dispose() = timer.Dispose()


let rt = new RandomTicker(1000)

rt.RandomTick.Add(fun nextInterval -> printfn "Tick, next = %A" nextInterval)


