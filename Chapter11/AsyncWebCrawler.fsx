﻿open System.Net
open System.Text.RegularExpressions
open System.Threading

let limit = 50

let linkPat = "href=\s*\"[^\"h]*(http://[^&\"]*)\""

let getLinks (txt : string) = 
    [ for m in Regex.Matches(txt, linkPat) -> m.Groups.Item(1).Value ]

// A type that helps limit the number of active web requests
type RequestGet(n:int) =
    let semaphore = 
        new Semaphore(initialCount = n, maximumCount = n)
    
    member x.AsyncAquire(?timeout) = 
        async {
            let! ok = Async.AwaitWaitHandle(semaphore, ?millisecondsTimeout = timeout)
            if ok then 
                return {
                    new System.IDisposable with
                        member x.Dispose() = semaphore.Release() |> ignore 
                        }
            else
                return! failwith "couldn't acquire a semaphore"
        }

// Gate the number of active web requests
let webRequestGate = RequestGet(5)

// Fetch the URL and post the results to the urlCollector
let collectLinks (url : string) = 
    async {
        // An Async web request with a global gate
        let! html = 
            async {
                // Acquire an entry in the webRequestGate. Release
                // it when 'holder' goes out of scope
                use! holder = webRequestGate.AsyncAquire()

                let req = WebRequest.Create(url, Timeout=5)

                // Wait for the WebResponse
                use! response = req.AsyncGetResponse()

                // Get the response stream
                use reader = new System.IO.StreamReader(response.GetResponseStream())

                // Read the response stream (note: a synchronous read)
                return reader.ReadToEnd()
            }

        // Compute the links synchronously
        let links = getLinks html

        // Report synchronously
        printfn "finished reading %s, got %d links" url (List.length links)

        return links
    }

// 'urlCollector' is a single gent that receives URLs as messages. It creates new
// asychronous tasks that post messages back to this object.
let urlCollector = 
    MailboxProcessor.Start(fun self ->
        // This is the main state of the urlCollector
        let rec waitForUrl (visited : Set<string>) =
            async {
                // Check the limit
                if visited.Count < limit then
                    // Wait for a URL
                    let! url = self.Receive()

                    if not (visited.Contains(url)) then
                        // Start off a new task for the new url. Each collects
                        // links and posts them back to the urlCollector.
                        do! Async.StartChild(async { 
                            let! links = collectLinks url
                            for link in links do
                                self.Post link 
                            }) |> Async.Ignore

                    return! waitForUrl(visited.Add(url))
            }

            // This is the initial state.
        waitForUrl(Set.empty)
    )

urlCollector.Post "http://news.google.com";;