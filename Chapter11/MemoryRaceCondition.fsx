﻿let lock (lockobj : obj) f =
    System.Threading.Monitor.Enter lockobj
    try
        f()
    finally
        System.Threading.Monitor.Exit lockobj

let readLock (rwLock : System.Threading.ReaderWriterLock) f =
    rwLock.AcquireReaderLock(System.Threading.Timeout.Infinite)
    try
        f()
    finally
        rwLock.ReleaseReaderLock()

let writeLock (rwLock : System.Threading.ReaderWriterLock) f =
    rwLock.AcquireWriterLock(System.Threading.Timeout.Infinite)
    try
        f()
        System.Threading.Thread.MemoryBarrier()
    finally
        rwLock.ReleaseWriterLock()


type MutablePair<'T, 'U>(x : 'T, y: 'U) =
    let mutable currentX = x
    let mutable currentY = y

    let rwLock = new System.Threading.ReaderWriterLock()

    member p.Value = (currentX, currentY)
    member p.Update(x, y) =
        // Race condition: This pair of updates is not atomic
        currentX <- x
        currentY <- y

    member p.Value2 =
        readLock rwLock (fun() -> (currentX, currentY))
        
    member p.Update2(x, y) =
        writeLock rwLock (fun () -> 
            currentX <- x
            currentY <- y
            )

let p = new MutablePair<_,_>(1, 2)
do Async.Start (async {
    do (while true do p.Update(10, 10))
    })
do Async.Start (async {
    do (while true do p.Update(20, 20))
    })

do Async.Start (async {
    do (while true do lock p (fun () -> p.Update(10, 10)))
    })
do Async.Start (async {
    do (while true do lock p (fun () -> p.Update(20, 20)))
    })