﻿open System.IO

let watcher = new FileSystemWatcher(__SOURCE_DIRECTORY__, EnableRaisingEvents = true)

watcher.Changed.Add(fun args -> printfn "File %s has changed!" args.Name)


[<EntryPoint>]
let main argv = 
    printfn "%A" argv
    0 // return an integer exit code
