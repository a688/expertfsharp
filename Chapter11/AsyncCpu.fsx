﻿open System
open System.Threading

let parallelArrayInit n f = 
    let currentLine = ref -1
    let res = Array.zeroCreate n
    let rec loop () =
        let y = Interlocked.Increment(currentLine)
        if y < n then res.[y] <- f y; loop()

    // Start just the right number of tasks, one for each physical CPU
    Async.Parallel [for i in 1..Environment.ProcessorCount -> async { do loop() }]
        |> Async.Ignore
        |> Async.RunSynchronously
    res

let rec fib x = if x < 2 then 1 else fib (x - 1) + fib (x - 2)

parallelArrayInit 25 fib;;



let forkJoinParallel(taskSeq) = 
    Async.FromContinuations (fun (cont, econt, ccont) ->
        let tasks = Seq.toArray taskSeq
        let count = ref tasks.Length
        let results = Array.zeroCreate tasks.Length
        
        tasks 
        |> Array.iteri (fun i p -> 
            Async.Start(
                async {
                    let! res = p 
                    results.[i] <- res;

                    let n = System.Threading.Interlocked.Decrement(count)

                    if n = 0 then cont results
                    }
            )         
        )
    )