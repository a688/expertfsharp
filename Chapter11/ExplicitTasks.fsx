﻿
open System.Threading
open System.Threading.Tasks

// Simulate doing something
let doSomething () =
    printfn "doing something normal 1..."
    System.Threading.Thread.Sleep 100
    printfn "done something normal 2..."

// Creating a task
let task = Task.Run doSomething

// A cancellable operation
let doSomethingCancellable (ct: CancellationToken) = 
    printfn "doing something cancellable..."
    ct.ThrowIfCancellationRequested()
    System.Threading.Thread.Sleep 100
    ct.ThrowIfCancellationRequested()
    printfn "done something cancellable..."

// Create a handle fo rcancellation
let cts = new CancellationTokenSource()

// Start the task
let task2k = Task.Run (fun() -> doSomethingCancellable(cts.Token))

// Attempt to cancel the task
cts.Cancel();;