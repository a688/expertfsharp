﻿open System.Net
open System.IO

let museums = 
    [ 
        "MOMA", "http://moma.org/"; 
        "British Museum", "http://www.thebritishmuseum.ac.uk/";
        "Prado", "http://www.museodelprado.es/"
    ]

let tprintfn fmt = 
    printf "[Thread %d]" System.Threading.Thread.CurrentThread.ManagedThreadId
    printfn fmt

let fetchAsync (nm, url : string) =
    async {
        tprintfn "Creating request for %s..." nm
        let req = WebRequest.Create(url)

        let! resp = req.AsyncGetResponse()

        tprintfn "Getting response for stream %s..." nm

        let stream = resp.GetResponseStream()

        tprintfn "Reading response for %s..." nm

        let reader = new StreamReader(stream)
        let html = reader.ReadToEnd()

        tprintfn "Read %d characters for %s..." html.Length nm        
        }

Async.Parallel 
    [for nm, url in museums -> fetchAsync(nm, url)]
    |> Async.Ignore
    |> Async.RunSynchronously
