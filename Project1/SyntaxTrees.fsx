﻿type Expr =             // mathmatical expression
    | Add of Expr * Expr
    | Bind of string * Expr * Expr
    | Var of string
    | Num of int

type Env = Map<string, int>

let rec eval (env : Env) expr =
    match expr with
    | Add (e1, e2) -> eval env e1 + eval env e2
    | Bind (var, rhs, body) -> eval (env.Add(var, eval env rhs)) body   // not tail recursive
    | Var var -> env.[var]
    | Num n -> n

// tail recursive eval using continuations
let rec evalCont (env : Env) expr cont =
    match expr with
    | Add (e1, e2) -> 
        evalCont env e1 (fun v1 -> 
        evalCont env e2 (fun v2 -> cont (v1 + v2))
        )
    | Bind (var, rhs, body) -> 
        evalCont env rhs (fun v1 -> 
        evalCont (env.Add(var, v1)) body cont)
    | Var var -> cont (env.[var])
    | Num n -> cont n

let contEval env expr = evalCont env expr (fun x -> x)