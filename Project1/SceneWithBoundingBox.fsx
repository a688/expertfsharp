﻿open System.Drawing

type SceneWithCachedBoundingBox =
    | EllipseInfo of RectangleF
    | RectInfo of RectangleF
    | CompositeRepr of SceneWithCachedBoundingBox list * RectangleF option ref

    member x.BoundingBox =
        match x with
        | EllipseInfo rect | RectInfo rect -> rect
        | CompositeRepr (scenes, cache) ->
            match !cache with
            | Some v -> v
            | None ->
                let bbox =
                    scenes
                    |> List.map (fun s -> s.BoundingBox)
                    |> List.reduce (fun r1 r2 -> RectangleF.Union(r1, r2))
                cache := Some bbox 
                bbox

    /// Create a composite node with an initially empty cache
    static member Composite scenes = 
        CompositeRepr(scenes, ref None)

    static member Ellipse rect = EllipseInfo rect
    static member Rect rect = CompositeRepr rect
