﻿// P AND Q, P or Q, NOT P
//type NonCachedProp =
//    | And of NonCachedProp * NonCachedProp
//    | Or of NonCachedProp * NonCachedProp
//    | Not of NonCachedProp
//    | Var of string
//    | True

type Prop =
    | Prop of int
and PropRepr =
    | AndRepr of Prop * Prop
    | OrRepr of Prop * Prop
    | NotRepr of Prop
    | VarRepr of string
    | TrueRepr

open System.Collections.Generic

module PropOps =
    let uniqStamp = ref 0
    type internal PropTable() =
        let fwdTable = new Dictionary<PropRepr, Prop>(HashIdentity.Structural)
        let bwdTable = new Dictionary<int, PropRepr>(HashIdentity.Structural)

        member t.ToUnique repr =
            if fwdTable.ContainsKey repr then fwdTable.[repr]
            else 
                let stamp = incr uniqStamp; !uniqStamp
                let prop = Prop stamp
                fwdTable.Add (repr, prop)
                bwdTable.Add (stamp, repr)
                prop

        member t.FromUnique (Prop stamp) =
            bwdTable.[stamp]

    let internal table = PropTable()

    // Public constructon functions
    let And (p1, p2) = table.ToUnique (AndRepr(p1, p2))
    let Not p = table.ToUnique (NotRepr p)
    let Or (p1, p2) = table.ToUnique (OrRepr(p1, p2))
    let Var p = table.ToUnique (VarRepr p)
    let True = table.ToUnique TrueRepr
    let False = Not True

    // Deconstruction function
    let getRepr p = table.FromUnique p

    // Listing 9-4
    let (|And|Or|Not|Var|True|) prop =
        match table.FromUnique prop with
        | AndRepr (x, y) -> And (x, y)
        | OrRepr (x, y) -> Or (x, y)
        | NotRepr x -> Not x
        | VarRepr v -> Var v
        | TrueRepr -> True

    let rec showProp precendence prop =
        let parenIfPrec lim s = if precendence < lim then "(" + s + ")" else s
        match prop with
        | Or (p1, p2) -> parenIfPrec 4 (showProp 4 p1 + " || " + showProp 4 p2)
        | And (p1, p2) -> parenIfPrec 3 (showProp 3 p1 + " && " + showProp 3 p2)
        | Not p -> parenIfPrec 2 ("not " + showProp 1 p)
        | Var v -> v
        | True -> "T"

    let rec nnf sign prop =
        match prop with 
        | And (p1, p2) -> 
            if sign then And (nnf sign p1, nnf sign p2)
            else Or (nnf sign p1, nnf sign p2)
        | Or (p1, p2) -> 
            if sign then Or (nnf sign p1, nnf sign p2)
            else And (nnf sign p1, nnf sign p2)
        | Not p -> nnf (not sign) p
        | Var _ | True -> if sign then prop else Not prop

    let NNF prop = nnf true prop

    //fsi.AddPrinter(showProp 5);;
let t1 = PropOps.Not (PropOps.And (PropOps.Not (PropOps.Var "x"), PropOps.Not (PropOps.Var "y")));;

let t2 = PropOps.Or(PropOps.Not(PropOps.Not(PropOps.Var("x"))),PropOps.Var("y"));;

PropOps.NNF t1;;
PropOps.NNF t2;;