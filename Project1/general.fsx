﻿// These two statements are functionally equivilent
List.fold (fun acc x -> acc + x) 0 [4;5;6]
(0, [4;5;6]) ||> List.fold (fun acc x -> acc + x)
// functions with two arguments can be passed in w/o defining the parameters
(0, [4;5;6]) ||> Seq.fold(+)

// All three of these are equivilents
[4;5;6] |> Seq.sum 
[4;5;6] |> Seq.fold(+) 0
[4;5;6] |> Seq.reduce(+)


// This returns 3 because the accumulator doesn't accually accumulate, it simply
// bring a value back through the function
([4;5;6;3;5], System.Int32.MaxValue) ||> List.foldBack(fun x acc -> min x acc);;
//val it : int = 3

// the >> operator Composes two functions (forward composition operator).
([(3, "three"); (5, "five")], System.Int32.MaxValue) ||> List.foldBack (fst >> min);;


let firstTwoLines file =
    seq { use s = System.IO.File.OpenText(file)
          yield s.ReadLine()
          yield s.ReadLine()
        }

System.IO.File.WriteAllLines("test1.txt", [|"line1";"line2";"line3";"line4";"line5"|])

// This will always read in just the first two lines of the file
let twoLines () = firstTwoLines "test1.txt"

