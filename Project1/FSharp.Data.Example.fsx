﻿
let sites = ["http://www.bing.com/" ; "http://www.google.com/" ]


let fetchResults = List.map fetch sites

List.map (fun (_,p) -> String.length p) fetchResults


let delimiters = [| ' '; '\n'; '\t'; '<'; '>'; '=' |]
let getWords (s: string) = s.Split delimiters

let getStats site =
    let url = "http://" + site
    let html = http url
    let hwords = html |> getWords
    let hrefs = hwords |> Array.filter (fun s -> s = "href")
    (site, html.Length, hwords.Length, hrefs.Length)

open System.Drawing

let remap (r1: RectangleF) (r2:RectangleF) =
    let scalex = r2.Width / r1.Width
    let scaley = r2.Height / r1.Height
    let mapx x = r2.Left + (x - r1.Left) * scalex
    let mapy y = r2.Top + (y - r1.Top) * scaley
    let mapp (p: PointF) = PointF(mapx p.X, mapy p.Y)
    mapp

let rect1 = RectangleF(100.0f, 100.0f, 100.0f, 100.0f)
let rect2 = RectangleF(50.0f, 50.0f, 50.0f, 50.0f)

let mapp = remap rect1 rect2

let time f =
    let startTime = System.DateTime.Now
    let res = f()
    let endTime = System.DateTime.Now
    (res, endTime - startTime)

time (fun () -> http "http://www.newscientist.com/")

let mapFirst = List.map fst
let mapFirst2 inp = List.map fst inp