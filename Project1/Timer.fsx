﻿
module public GlobalClock =
    type TickTock = Tick | Tock

    let mutable private clock = Tick

    let private tick = new Event<TickTock>()

    let internal oneTick() =
        (clock <- match clock with Tick -> Tock | Tock -> Tick)
        tick.Trigger (clock)

    let tickEvent = tick.Publish

module internal TickTockDriver =
    open System.Threading

    let timer = new Timer(callback = (fun _ -> GlobalClock.oneTick()), state = null, dueTime = 0, period = 100)

module TickTockLisitener =
    GlobalClock.tickEvent.Add(
        function
        | GlobalClock.Tick -> printfn "tick!"
        | GlobalClock.Tock -> printfn "tock!"
        )