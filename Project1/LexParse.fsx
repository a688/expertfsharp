﻿// Tokenizer
type Token = 
    | ID of string
    | INT of int
    | HAT
    | PLUS
    | MINUS

let regex s = new System.Text.RegularExpressions.Regex(s)

let tokenR = regex @"((?<token>(\d+|\w+|\^|\+|-))\s*)*"

let tokenize (s : string) =
    [ for x in tokenR.Match(s).Groups.["token"].Captures do
        let token =
            match x.Value with
            | "^" -> HAT
            | "-" -> MINUS
            | "+" -> PLUS
            | s when System.Char.IsDigit s.[0] -> INT (int s)
            | s -> ID s
        yield token]

tokenize "x^5 - 2x^3 + 20";;

// x^5 - 2x^3 + 20
// Represented as [Term (1, "x", 5); Term(-2, "x", 3); Const 20]
type Term =
    | Term of int * string * int
    | Const of int

type Polynomial = Term list
type TokenStream = Token list

// Returns the next token from the list of tokens
let tryToken (src : TokenStream) = 
    match src with
    | tok :: rest -> Some(tok, rest)
    | _ -> None

// Parses the exponent value. If there is a HAT character then the
// next token needs to be an INT #. If there is no hat then the 
// exponent is 1 by default
let parseIndex src = 
    match tryToken src with 
    | Some (HAT, src) -> 
        match tryToken src with
        | Some (INT num2, src) -> num2, src
        | _ -> failwith "expected an integer after '^'"
    | _ -> 1, src

// Parses out the Term from the src
// Check to see if the first item is either an INT or an identifier (string)
// If int then try to get the identifier, if there is no identifier then
// this term is just a constant. If there is an identifier then try
// to get the exponent (if there is one)
let parseTerm src = 
    match tryToken src with
    | Some (INT num, src) ->        // Matches if this token is an INT
        match tryToken src with
        | Some (ID id, src) ->      // Matches if this token is an ID
            let idx, src = parseIndex src 
            Term(num, id, idx), src
        | _-> Const num, src
    | Some (ID id, src) ->          // Matches if this token is an ID
        let idx, src = parseIndex src
        Term(1, id, idx), src
    | _ -> failwith "end of token stream in term"

// Builds a list of term items. Terms are split by + or - (PLUS or
// MINUS) symbols. This function discards operators so it isn't completely
// functional
let rec parsePolynomial src =
    let t1, src = parseTerm src
    match tryToken src with
    | Some (PLUS, src) -> 
        let p2, src = parsePolynomial src 
        (t1::p2), src
    | Some (MINUS, src) -> 
        let p2, src = parsePolynomial src 
        (t1::p2), src
    | _ -> [t1], src

// Parses an input string into terms. An exception is thrown
// if there are left over items after parsing is complete
let parse input = 
    let src = tokenize input
    let result, src = parsePolynomial src
    match tryToken src with
    | Some _ -> failwith "unexpected input at end of token stream!"
    | None -> result

parse "x^5 - 2x^3 + 20";;

