﻿#r @"C:\Users\jalon\Source\Repos\ExpertFSharp\packages\FSharp.Data.2.2.5\lib\net40\FSharp.Data.dll";;

open FSharp.Data;;
open FSharp.Data.JsonExtensions;;

let animals = 
    JsonValue.Parse """
        { "dogs":   
            [   { "category": "Companion dogs", "name": "Chihuahua" },
                { "category": "Hounds", "name": "Foxhound" }
            ]
        }"""

//let data2 = JsonValue.Load(__SOURCE_DIRECTORY__ + "data/file.json")

//let data3 = JsonValue.Load("http://api.worldbank.org/country/cz/indicator/GC.DOD.TOTL.GD.ZA?format=json")

let dogs = [ for dog in animals?dogs -> dog?name ]

// Define the type/parser
type Customers = JsonProvider<"""
    { "customers" :
        [ 
            { 
                "name" : "ACME",
                "orders" :
                    [ { "number": "A012345", "item" : "widget", "quantity" : 1 } ]
            } 
        ] 
    }""">

// Use the parser (aka parse) data into a local variable
let customers = 
    Customers.Parse """
        { "customers" :
            [ 
                { "name" : "Apple Store",
                    "orders" :
                        [
                            { "number" : "B73284", "item" : "iphone5", "quantity" : 18373 },
                            { "number" : "B73238", "item" : "iphone6", "quantity" : 3736 }
                        ]
                },
                { "name" : "Samsung Shop",
                    "orders" :
                        [ { "number" : "N36214", "item" : "nexus7", "quantity" : 18373 } ]
                }
            ]
        }"""
 
// Do something with the data
let customerNames = [ for c in customers.Customers -> c.Name ]

let newOrder = Customers.Order(number = "N36214", item = "nexus7", quantity = 1636)

let newCustomer = Customers.Customer(name = "FabPhone", orders = [| newOrder |])

let jsonText = newCustomer.JsonValue.ToString()
