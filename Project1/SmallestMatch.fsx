﻿
let numbersToMatch = Seq.init 20 (fun i -> i + 1)

// we will step by 10 so all numbers will be divisible by 1, 2, 5, and 10 (5.67 seconds)
let NumbersToMatchOptimized = List.init 16 (fun i -> 
                                                match i with
                                                | _ when i < 2 -> i + 3
                                                | _ when i < 6 -> i + 4
                                                | _ -> i + 5)

let timeIt f =
    let startTime = System.DateTime.Now
    let res = f()
    let endTime = System.DateTime.Now
    (res, endTime - startTime)

let rec FindSmallestMatch (toMatch : List<int>) (acc : int) : int =
    let matches = toMatch |> List.filter ( fun x -> acc % x = 0 )
    match matches.Length = toMatch.Length with
    | true -> acc
    | false -> FindSmallestMatch toMatch (acc + 5)

let rec FindSmallestMatch2 (toMatch : List<int>) (acc : int) : int =
    let sum = toMatch |> List.fold ( fun acc2 x -> acc2 + (acc % x) ) 0
    match sum = 0 with
    | true -> acc
    | false -> FindSmallestMatch2 toMatch (acc + 5)

//timeIt (fun() -> FindSmallestMatch numbersToMatch 5)
timeIt (fun() -> FindSmallestMatch NumbersToMatchOptimized 10)
timeIt (fun() -> FindSmallestMatch2 NumbersToMatchOptimized 10)



