﻿#r @"C:\Users\jalon\Source\Repos\ExpertFSharp\packages\FSharp.Data.2.2.5\lib\net40\FSharp.Data.dll";;
#r "System.Xml.Linq";;

[<Literal>]
let customersXmlSample = """
    <Customers>
        <Customer name="ACME">
            <Order Number="A012345">
                <OrderLine Item="widget" Quantity="1" />
            </Order>
            <Order Number="A012346">
                <OrderLine Item="trinket" Quantity="2" />
            </Order>
        </Customer>
        <Customer name="Southwind">
            <Order Number="A012347">
                <OrderLine Item="skyhook" Quantity="3" />
                <OrderLine Item="gizmo" Quantity="4" />
            </Order>
        </Customer>
    </Customers>"""

type InputXml = FSharp.Data.XmlProvider<customersXmlSample>

let inputs = InputXml.GetSample().Customers

let orders =
    [for customer in inputs do
        for order in customer.Orders do
            for line in order.OrderLines do
                yield (customer.Name, order.Number, line.Item, line.Quantity)
    ]

[<Literal>]
let orderLinesXmlSample = """
    <OrderLines>
        <OrderLine Customer="ACME" Order="A012345" Item="widget" Quantity="1" />
        <OrderLine Customer="ACME" Order="A012346" Item="trinket" Quantity="2" />
        <OrderLine Customer="Southwind" Order="A012347" Item="skyhook" Quantity="3" />
        <OrderLine Customer="Southwind" Order="A012347" Item="gizmo" Quantity="4" />
    </OrderLines>"""

type OutputXml = FSharp.Data.XmlProvider<orderLinesXmlSample>    

let orderLines = 
    OutputXml.OrderLines
        [| for (name, number, item, quantity) in orders do
                yield OutputXml.OrderLine(name, number, item, quantity)
        |]

orderLines.XElement.ToString()