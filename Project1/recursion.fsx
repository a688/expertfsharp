﻿// this will overflow 
let rec deepRecursion n =
    if n = 1000000 then () else
    if n % 100 = 0 then
        printfn "--> deppRecursion, n = %d" n
    deepRecursion (n+1)
    printfn "<-- deepRecursion, n = %d" n // this line is responsible for the non tail recursion

deepRecursion 0;;


let rec tailRecursion n =
    if n = 1000000 then () else // removing this line would cause this function to run forever
    if n % 100 = 0 then
        printfn "--> deppRecursion, n = %d" n
    tailRecursion (n+1)

tailRecursion 0;;



type Tree =
    | Node of string * Tree * Tree
    | Tip of string
    
let rec sizeNotTailRecursive tree =
    match tree with
    | Tip _ -> 1
    | Node (_, leftT, rightT) -> 
        sizeNotTailRecursive leftT + sizeNotTailRecursive rightT

// Makes an unbalanced (right overloaded) tree
let rec mkBigUnbalancedTree n tree =
    if n = 0 then tree
    else Node("node", Tip("tip"), mkBigUnbalancedTree (n - 1) tree)

// Creates a very unblanaced (right overloaded) tree
let tree1 = Tip("tip")
let tree2 = mkBigUnbalancedTree 15000 tree1
let tree3 = mkBigUnbalancedTree 15000 tree2
let tree4 = mkBigUnbalancedTree 15000 tree3
let tree5 = mkBigUnbalancedTree 15000 tree4
let tree6 = mkBigUnbalancedTree 15000 tree5


// Specialized (and unrealistic) right biased tree size checker
let rec sizeAcc acc tree =
    match tree with
    | Tip _ -> 1 + acc
    | Node(_, lTree, rTree) ->
        let acc = sizeAcc acc lTree
        sizeAcc acc rTree

// Continuation based size function
let rec sizeCont tree cont =
    match tree with
    | Tip _ -> cont 1
    | Node(_, lTree, rTree) ->
        sizeCont lTree (fun leftSize -> 
            sizeCont rTree (fun rightSize -> 
                cont (leftSize + rightSize)
                )
            )

let size tree = sizeCont tree id


// Accumulator with explicit continuation
let rec sizeContAcc acc tree cont =
    match tree with
    | Tip _ -> cont (1 + acc)
    | Node (_, lTree, rTree) -> 
        sizeContAcc acc lTree (fun accLeftSize ->
            sizeContAcc accLeftSize rTree cont)

// Start with accumlator of 0
// Traverse the left side of the tree until a TIP is found, 
//  building up a continuation for each node along the way
// When a TIP is encountered, the continuation from the previous node
//  is called, with accLeftSize increased by 1. The continuation make 
//  recursive call to sizeContAcc for its right tree, passing the 
//  continuation for the second-to-last node along the way.
// When all is done, all of the left and right trees have been explored,
//  and the final result is delivered to the (fun x -> x) continuation.

let accContSize tree = sizeContAcc 0 tree id

