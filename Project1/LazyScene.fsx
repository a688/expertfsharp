﻿open System.Drawing
open System.Xml

type Scene =
    | Ellipse of RectangleF
    | Rect of RectangleF
    | Composite of Scene list
    | Delay of Lazy<Scene>

    /// A derived constructor
    static member Circle(center : PointF, radius) =
        Ellipse(RectangleF(center.X - radius, center.Y - radius, radius * 2.0f, radius * 2.0f))

    // A derived constructor
    static member Square(left, top, side) =
        Rect(RectangleF(left, top, side, side))


// Extract a number from an XML attribute collection
let extractFloat32 attrName (attribs : XmlAttributeCollection) =
    float32(attribs.GetNamedItem(attrName).Value)

// Extract a point from an XML attribute collection
let extractPointF (attribs : XmlAttributeCollection) =
    PointF(extractFloat32 "x" attribs, extractFloat32 "y" attribs)

// Extract a rectangle from an XML attribute collection
let extractRectangleF (attribs : XmlAttributeCollection) =
    RectangleF(
        extractFloat32 "left" attribs,
        extractFloat32 "top" attribs,
        extractFloat32 "width" attribs,
        extractFloat32 "height" attribs)

// Extract a scene from an XML node
let rec extractScene (node: XmlNode) = 
    let attribs = node.Attributes
    let childNodes = node.ChildNodes
    match node.Name with
    | "Circle" -> Scene.Circle(extractPointF(attribs), extractFloat32 "radius" attribs)
    | "Ellipse" -> Scene.Ellipse(extractRectangleF(attribs))
    | "Rectangle" -> Scene.Rect(extractRectangleF(attribs))
    | "Square" -> Scene.Square(
                    extractFloat32 "left" attribs, 
                    extractFloat32 "top" attribs, 
                    extractFloat32 "side" attribs)
    | "Composite" -> Scene.Composite [for child in childNodes -> extractScene(child)]
    | "File" ->
        let file = attribs.GetNamedItem("file").Value
        let scene = lazy (
            let d = XmlDocument()
            d.Load(file)
            extractScene(d :> XmlNode)
            )
        Scene.Delay scene
    | _ -> failwithf "unable to convert XML '%s'" node.OuterXml

let rec getScene scene =
    match scene with
    | Delay d -> getScene (d.Force())
    | _ -> scene