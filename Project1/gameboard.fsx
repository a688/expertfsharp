﻿let rand = System.Random()

let randomNumbers = seq { while true do yield rand.Next(10000) }

// 8x8 board
let gameBoard =
    [ for i in 0 .. 7 do
        for j in 0 .. 7 do
            yield (i, j, rand.Next(10))
    ]

let evenPositions = 
    gameBoard
    |> Seq.choose( fun (i, j, v) -> if v% 2 = 0 then Some (i,j) else None)
    |> Seq.toList

let firstElementScoringZero =
    gameBoard
    |> Seq.tryFind ( fun (_, _, v) -> v = 0)

let firstPositionScoringZero = 
    gameBoard
    |> Seq.tryPick (fun (i, j, v) -> if v = 0 then Some(i, j) else None)

let positionsGroupByGameValue =
    gameBoard 
    |> Seq.groupBy (fun (_, _, v) -> v)
    |> Seq.sortBy (fun (k, _) -> k)
    |> Seq.toList

let positionsIndexedByGameValue = 
    gameBoard
    |> Seq.groupBy (fun (_, _, v) -> v)
    |> Seq.sortBy (fun (k, _) -> k)
    |> Seq.map (fun (k, v) -> (k, Seq.toList v))
    |> dict

let worstPositions = positionsIndexedByGameValue.[0]
let bestPositions = positionsIndexedByGameValue.[9]
