﻿let splitAtSpaces(line : string) =
    line.Split([| " " |], System.StringSplitOptions.RemoveEmptyEntries)
    |> Array.toList

let wordCount text =
    let words = splitAtSpaces text
    let distinctWords = words |> List.distinct
    (words.Length, words.Length - distinctWords.Length)

let showWordCount text = 
    let numWords, dupCount = wordCount text
    printfn "--> %d words in the text" numWords
    printfn "--> %d duplicate words" dupCount

showWordCount "all the king horses and all the kings men"


#r "C:/Users/jalon/Documents/Visual Studio 2015/Projects/ExpertF/packages/FSharp.Data.2.2.5/lib/net40/FSharp.Data.dll"

open FSharp.Data

type Species = HtmlProvider<"http://en.wikipedia.org/wiki/The_world's_100_most_threatened_species">

let species =
    [ for x in 
        Species.GetSample().Tables.``Species list``.Rows -> x.Type, x.``Common name`` ]

