﻿let squares = seq { for i in 0 .. 10 -> (i, i * i) }

let cubed = seq { for (i, iSquared) in squares -> (i, iSquared, iSquared * i )}

let rec allFiles dir =
    seq { 
        for file in System.IO.Directory.GetFileSystemEntries dir do
            yield file

        for subdir in System.IO.Directory.GetDirectories dir do
            yield! allFiles subdir
    }