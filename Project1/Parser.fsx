﻿type CodeBlockFunction = System.Collections.Generic.Dictionary<string, string> * array<string> -> string 
type CodeBlockParameters = array<string>
   
type ParserState =
    | FunctionName
    | Parameter
    | StaticText

type ScriptBlock =
    | StaticText of string
    | CodeBlock of CodeBlockFunction * CodeBlockParameters

let ProcessTextToken(institution : System.Collections.Generic.Dictionary<string, string>, parameters : CodeBlockParameters) =
    "process text token"

let ProcessNumberToken(institution : System.Collections.Generic.Dictionary<string, string>, parameters : CodeBlockParameters) =
    "process number token"

let ProcessInvalidToken(institution : System.Collections.Generic.Dictionary<string, string>, parameters : CodeBlockParameters) =
    "fuck!"

let parseCodeBlock (block : string) : CodeBlockFunction * CodeBlockParameters =
    let parts = block.Split([| "|" |], System.StringSplitOptions.None)
    let parameters = parts.[1].Split([| "," |], System.StringSplitOptions.None)

    let parser =
        match parts.[0].ToUpperInvariant() with 
        | "PARSETEXT" -> ProcessTextToken
        | "PARSENUMBER" -> ProcessNumberToken
        | _ -> ProcessInvalidToken

    parser, parameters

let ParseScript (script : string) =
    let parts = new System.Collections.Generic.List<ScriptBlock>()
    let mutable state = ParserState.StaticText
    let mutable idx = 0

    let text = new System.Text.StringBuilder()

    while idx < script.Length do
        match script.[idx], state with
        | '#', ParserState.StaticText -> 
                parts.Add( StaticText (text.ToString()) )
                ignore ( text.Clear() )
                state <- ParserState.FunctionName
        | '#', _ -> 
                parts.Add( CodeBlock (parseCodeBlock(text.ToString()) ) )
                ignore ( text.Clear() )
                state <- ParserState.StaticText
        | '(', ParserState.FunctionName -> 
                ignore ( text.Append("|") )
                state <- ParserState.Parameter
        | ')', ParserState.Parameter -> ignore ()
        | _ -> ignore ( text.Append(script.[idx]) )

        idx <- idx + 1

    parts.Add(StaticText ( text.ToString() ))

    parts.ToArray()

let myScript = @"<haoola>#ParseText(21134)# this is just,a,fbig, whatnot) #ParseNumber(133)# nmbersa re dumb!!"

let myData = new System.Collections.Generic.Dictionary<string, string>()

ParseScript myScript
    |> Array.map( fun item -> 
            match item with 
            | ScriptBlock.CodeBlock (f,p) -> f(myData, p)
            | ScriptBlock.StaticText (t) -> t
        )
    |> Array.reduce( fun acc t -> acc + t )
    