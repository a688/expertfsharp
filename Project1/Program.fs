﻿module prog

type SampleClass(key : int, value: string) =
    let elements = new System.Collections.Generic.Dictionary<int, string>()

    do elements.Add(key, value)

    member t.Item
        with get(idx) =
            if (elements.ContainsKey(idx)) then
                elements.[idx]
            else
                ""

        and set (idx) newValue =
            if (elements.ContainsKey(idx)) then
                elements.[idx] <- newValue
            else
                elements.Add(idx, newValue)

let myClassTester =
    let mc = new SampleClass(5, "13")
    printfn "%A" mc.[5]
    mc.[5] <- "new value"
    printfn "%A" mc.[5]

let splitAtSpaces(line : string) =
    line.Split([| " " |], System.StringSplitOptions.RemoveEmptyEntries)
    |> Array.toList

let wordCount text =
    let words = splitAtSpaces text
    let distinctWords = words |> List.distinct
    (words.Length, words.Length - distinctWords.Length)

let showWordCount text = 
    let numWords, dupCount = wordCount text
    printfn "--> %d words in the text" numWords
    printfn "--> %d duplicate words" dupCount

let functionFromBranch =
    "is this in master now?"

