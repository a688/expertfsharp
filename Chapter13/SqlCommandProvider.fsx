﻿#r @"C:\Users\jalon\Source\Repos\ExpertFSharp\packages\FSharp.Data.SqlClient.1.8.2\lib\net40\FSharp.Data.SqlClient.dll"

open FSharp.Data

[<Literal>]
let connectionString = @"Data Source=SISYPHUS\SQLEXPRESS;Initial Catalog=AdventureWorks2014;Integrated Security=True"

let cmd = new SqlCommandProvider<"Select Top (@topN) FirstName, LastName, SalesYTD From Sales.vSalesPerson Where CountryRegionName = @regionName and SalesYTD > @salesMoreThan Order By SalesYTD", connectionString>(connectionString)

cmd.Execute(topN = 3L, regionName = "United States", salesMoreThan = 1000000M) |> printfn "%A"

