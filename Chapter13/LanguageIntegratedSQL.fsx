﻿#r @"C:\Users\jalon\Source\Repos\ExpertFSharp\packages\FSharp.Data.TypeProviders.5.0.0.2\lib\net40\FSharp.Data.TypeProviders.dll"
#r "System.Data.Linq.dll"

//

open FSharp.Linq
open FSharp.Data.TypeProviders

type NorthwndDb = 
    SqlDataConnection<ConnectionString = @"Data Source=SISYPHUS\SQLEXPRESS;Initial Catalog=NORTHWND;Integrated Security=True", Pluralize= true>

let db = NorthwndDb.GetDataContext()

db.DataContext.Log <- System.Console.Out;;

let customersSortedByCountry = 
    query { 
        for c in db.Customers do
            sortBy c.Country
            select (c.Country, c.CompanyName) }
    |> Seq.toList

let selectedEmployees =
    query {
        for emp in db.Employees do
            where (emp.BirthDate.Value.Year > 1960)
            where (emp.LastName.StartsWith "S")
            select (emp.FirstName, emp.LastName)
            take 5 }
    |> Seq.toList

let customersSortedTwoColumns = 
    query {
        for c in db.Customers do
            sortBy c.Country
            thenBy c.Region
            select (c.Country, c.Region, c.CompanyName) }
    |> Seq.toList

let totalOrderQuantity =
    query {
        for order in db.OrderDetails do
            sumBy (int order.Quantity) }
    
let customersAverageOrders = 
    query { 
        for c in db.Customers do
            averageBy (float c.Orders.Count) }


let averagePriceOverProductRange =
    query {
        for p in db.Products do
            averageByNullable p.UnitPrice }


let totalOrderQuantity = 
    query {
        for c in db.Customers do
            let numOrders = 
                query {
                    for o in c.Orders do
                        for od in o.OrderDetails do
                            sumByNullable(System.Nullable(int od.Quantity)) }
            let averagePrice = 
                query {
                    for o in c.Orders do
                        for od in o.OrderDetails do
                            averageByNullable(System.Nullable(od.UnitPrice)) }
            select (c.ContactName, numOrders, averagePrice) }
    |> Seq.toList

let productsGroupedByNameAndCountedTest1 = 
    query { 
        for p in db.Products do
            groupBy p.Category.CategoryName into group
            let sum = 
                query {
                    for p in group do
                        sumBy (int p.UnitsInStock.Value) }
            select (group.Key, sum) }
    |> Seq.toList


let innerJoinQuery =
    query {
        for c in db.Categories do
            join p in db.Products on (c.CategoryID =? p.CategoryID)
            select (p.ProductName, c.CategoryName) }
    |> Seq.toList

let innerjoinQuery2 = 
    query { 
        for p in db.Products do
            select (p.ProductName, p.Category.CategoryName) }
    |> Seq.toList

let innerGroupJoinQueryWithAggregation =
    query { 
        for c in db.Categories do
            groupJoin p in db.Products on (c.CategoryID =? p.CategoryID) into prodGroup
            let groupMax = 
                query {
                    for p in prodGroup do maxByNullable p.UnitsOnOrder }
            select (c.CategoryName, groupMax) }
    |> Seq.toList



