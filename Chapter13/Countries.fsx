﻿#r "System.Globalization.dll"
#r "System.Numerics.dll"
#r "System.Xml.Linq.dll"
#r "System.Xml.XDocument.dll"
#r "C:\Users\jalon\Source\Repos\ExpertFSharp\packages\FSharp.Data.2.3.2\lib\portable-net45+netcore45\FSharp.Data.dll"

open FSharp.Data

type CountriesXml = XmlProvider<"http://api.worldbank.org/country">
type CountriesJson = JsonProvider<"http://api.worldbank.org/country?format=json">


let loadPageFromXml n = 
    CountriesXml.Load(sprintf "http://api.worldbank.org/country?page=%d" n)

let loadPageFromJson n = 
    CountriesJson.Load(sprintf "http://api.worldbank.org/country?format=json&page=%d" n)

//let sampleCountries = CountriesXml.GetSample();;
//let cNames = [ for c in countries -> c.Name ];;
//printfn "%A" cNames;;

let countries =
    let page1 = loadPageFromXml 1
    [ for i in 1 .. page1.Pages do
        let page = loadPageFromXml i
        yield! page.Countries ]
   
let countriesJson =
    let page1 = loadPageFromJson 1
    [ for i in 1 .. page1.Record.Pages do
        let page = loadPageFromJson i
        yield! page.Array ]
    
printfn "XML Countries: %i" countries.Length;;
printfn "JSON Countries: %i" countriesJson.Length;;