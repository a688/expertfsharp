﻿type Var = string

type Prop =
    | And of Prop * Prop
    | Var of Var
    | Not of Prop
    | Exists of Var * Prop
    | False

// Derived constructs (derives meaning from previously defined things)
let True            = Not False
let Or(p, q)        = Not(And(Not(p), Not(q)))
let Iff(p, q)       = Or(And(p, q), And(Not(p), Not(q)))
let Implies(p, q)   = Or(Not(p), q)
let Forall(v,p)     = Not(Exists(v, Not(p)))

// Infix operators
let (&&&) p q       = And(p, q)
let (|||) p q       = Or(p,q)
let (~~~) p         = Not p
let (<=>) p q       = Iff(p, q)
let (===) p q       = (p <=> q)
let (==>) p q       = Implies(p, q)
let (^^^) p q       = Not (p <=> q)

// Used to build primitive prepositions
let var (nm: Var) = Var nm


// Used to generate fresh variables
let fresh = 
    let mutable count = 0
    fun nm -> 
        count <- count + 1
        (sprintf "_%s%d" nm count : Var)

// Naive logic

/// Computes the value of a formula given assignments for each variable that occurs in the formula
let rec eval (env: Map<Var, bool>) inp =
    match inp with
    | Exists(v, p) -> eval (env.Add(v, false)) p || eval (env.Add(v, true)) p
    | And(p1, p2) -> eval env p1 && eval env p2
    | Var v ->
        if env.ContainsKey(v) then env.[v]
        else failwithf "env didn't contain a value for %A" v
    | Not p -> not (eval env p)
    | False -> false

/// Computes the set of variables that occurs in the formulas
let rec support f = 
    match f with
    | And (x, y) -> Set.union (support x) (support y)
    | Exists(v, p) -> (support p).Remove(v)
    | Var p -> Set.singleton p
    | Not x -> support x
    | False -> Set.empty

let rec cases supp =
    seq {
        match supp with
        | [] -> yield Map.empty
        | v::rest ->
            yield! rest |> cases |> Seq.map (Map.add v false)
            yield! rest |> cases |> Seq.map (Map.add v true)
    }

let truthTable x =
    x |> support |> Set.toList |> cases |> Seq.map (fun env -> env, eval env x)

let satisfiable x =
    x
    |> truthTable
    |> Seq.exists (fun (env, res) -> res)

let satisfiableWithExample x =
    x
    |> truthTable
    |> Seq.tryFind (fun (env, res) -> res)
    |> Option.map fst

let tautology x =
    x
    |> truthTable
    |> Seq.forall (fun (env, res) -> res)

let tautologyWithCounterExample x =
    x
    |> truthTable
    |> Seq.tryFind(fun (env, res) -> not res)
    |> Option.map fst

let printCounterExample x =
    match x with
    | None -> printfn "tautology verified OK"
    | Some env -> printfn "tautology failed on %A" (Seq.toList env)



// For outputting answers
let stringOfBit b = if b then "T" else "F"
let stringOfEnv env = Map.fold(fun acc k v -> sprintf "%s=%s;" k (stringOfBit v) + acc) "" env
let stringOfLine (env, res) = sprintf "%20s %s" (stringOfEnv env) (stringOfBit res)
let stringOfTruthTable tt =
    "\n" + (tt |> Seq.toList |> List.map stringOfLine |> String.concat "\n")

fsi.AddPrinter(fun tt -> tt |> Seq.truncate 20 |> stringOfTruthTable);;

// satisfiable (var "x");;
// satisfiableWithExample (var "x");;
//truthTable (var "x" &&& var "y");;

// Says that X or NOT(x) is a tautology no matter what the actual value of "x" is
//tautology (var "x" ||| ~~~(var "x"));;



// 2bit adder
let sumBit x y = (x ^^^ y)
let carryBit x y = (x &&& y)
let halfAdder x y sum carry = 
    (sum === sumBit x y) &&& (carry === carryBit x y)

let fullAdder x y z sum carry = 
    let xy = (sumBit x y)
    (sum === sumBit xy z) &&& (carry === (carryBit x y ||| carryBit xy z))
let twoBitAdder (x1, x2) (y1, y2) (sum1, sum2) carryInner carry =
    (halfAdder x1 y1 sum1 carryInner) &&& (fullAdder x2 y2 carryInner sum2 carry)



// vector wires/bits instead of individual
type bit = Prop
type bitvec = bit[]

let Lo : bit = False
let Hi : bit = True

let vec n nm : bitvec = Array.init n (fun i -> var (sprintf "%s%d" nm i))
let bitEq (b1 : bit) (b2: bit) = (b1 <=> b2)
let AndL l = Seq.reduce (fun x y -> And(x, y)) l
let vecEq (v1 : bitvec) (v2 : bitvec) = AndL (Array.map2 bitEq v1 v2)

let fourBitAdder (x : bitvec) (y : bitvec) (sum : bitvec) (carry : bitvec) =
    halfAdder x.[0] y.[0] sum.[0] carry.[0] &&&
    fullAdder x.[1] y.[1] carry.[0] sum.[1] carry.[1] &&&
    fullAdder x.[2] y.[2] carry.[1] sum.[2] carry.[2] &&&
    fullAdder x.[3] y.[3] carry.[2] sum.[3] carry.[3]

let Blocks l = AndL l

// N-bit adder with halfAdder at one end
let nBitCarryRippleAdder (n : int) (x : bitvec) (y : bitvec) (sum : bitvec) (carry : bitvec) =
    Blocks [
        for i in 0..n-1 ->
            if i = 0 then halfAdder x.[i] y.[i] sum.[i] carry.[i]
            else fullAdder x.[i] y.[i] carry.[i - 1] sum.[i] carry.[i]
    ]

let rippleAdder (n : int) (x : bitvec) (y : bitvec) (sum : bitvec) (carry : bitvec) =
    Blocks [
        for i in 0..n-1 -> 
            fullAdder x.[i] y.[i] carry.[i] sum.[i] carry.[i+1] 
    ]

let towBitAdderWithHiding (x1, x2) (y1, y2) (sum1, sum2) carry =
    let carryInnerVar = fresh "carry"
    let carryInner = var(carryInnerVar)
    Exists(carryInnerVar, halfAdder x1 y1 sum1 carryInner &&& fullAdder x2 y2 carryInner sum2 carry)

//tautology (fullAdder Lo Lo Lo Lo Lo);;
//satisfiable (fullAdder Lo Lo Lo Hi Lo);;
//tautology (halfAdder (var "x") (var "x") Lo (var "x"));;