﻿open System.Collections.Generic

type Var = string

type Prop =
    | And of Prop * Prop
    | Var of Var
    | Not of Prop
    | Exists of Var * Prop
    | False

// Derived constructs (derives meaning from previously defined things)
let True            = Not False
let Or(p, q)        = Not(And(Not(p), Not(q)))
let Iff(p, q)       = Or(And(p, q), And(Not(p), Not(q)))
let Implies(p, q)   = Or(Not(p), q)
let Forall(v,p)     = Not(Exists(v, Not(p)))

// Infix operators
let (&&&) p q       = And(p, q)
let (|||) p q       = Or(p,q)           // Is P TRUE or Q TRUE, but not both TRUE
let (~~~) p         = Not p             // Is p FALSE
let (<=>) p q       = Iff(p, q)         // Are p & q the same (either both TRUE or both FALSE)
let (===) p q       = (p <=> q)         // Are p & q equivilent
let (==>) p q       = Implies(p, q)     // Does p imply q?
let (^^^) p q       = Not (p <=> q)     // Are p & q different


// Used to build primitive prepositions
let var (nm: Var) = Var nm

let memoize f = 
    let tab = new Dictionary<_,_>()
    fun x -> 
        if tab.ContainsKey(x) then tab.[x]
        else let res = f x in tab.[x] <- res; res
        // this could be written as
        //let rex = fx
        //  

type BddIndex = int
type Bdd = Bdd of BddIndex
type BddNode = Node of Var * BddIndex * BddIndex
type BddBuilder (order : Var -> Var -> int) =
    // The core data structures that preserve uniqueness
    let nodeToIndex = new Dictionary<BddNode, BddIndex>()
    let indexToNode = new Dictionary<BddIndex, BddNode>()

    // Keep track of the next indexes
    let mutable nextIdx = 2
    let trueIdx = 1
    let falseIdx = -1
    let trueNode = Node("", trueIdx, trueIdx)
    let falseNode = Node("", falseIdx, falseIdx)

    // Map indexes to nodes. Negative indexes go to their negation.
    // The special indexes (-1 and 1) go to special true/false nodes
    let idxToNode(idx) =
        if idx = trueIdx then trueNode
        elif idx = falseIdx then falseNode
        elif idx > 0 then indexToNode.[idx]
        else 
            let (Node(v, l, r)) = indexToNode.[-idx] 
            Node(v, -l, -r)
        
    // Map nodes to indexes. Add an entry to the table if needed
    let nodeToUniqueIdx(node) = 
        if nodeToIndex.ContainsKey(node) then nodeToIndex.[node]
        else 
            let idx = nextIdx
            nodeToIndex.[node] <- idx
            indexToNode.[idx] <- node
            nextIdx <- nextIdx + 1
            idx

    // Get the canonical idnex for a node. Preserve the invariant that the
    // Left-hand node of a conditional is always a positive number
    let mkNode(v : Var, l : BddIndex, r: BddIndex) =
        if l = r then l
        elif l >= 0 then nodeToUniqueIdx(Node(v, l, r))
        else -nodeToUniqueIdx(Node(v, -l, -r))

    // Construct the BDD for a conjunction "m1 AND m2"
    let rec mkAnd(m1, m2) =
        if (m1 = falseIdx || m2 = falseIdx) then falseIdx
        elif m1 = trueIdx then m2
        elif m2 = trueIdx then m1
        else
            let (Node(x, l1, r1)) = idxToNode(m1)
            let (Node(y, l2, r2)) = idxToNode(m2)
            let v, (la, lb), (ra, rb) =
                match order x y with 
                | c when c = 0 -> x, (l1, l2), (r1, r2)
                | c when c < 0 -> x, (l1, m2), (r1, m2)
                | c -> y, (m1, l2), (m1, r2)

            mkNode(v, mkAnd(la, lb), mkAnd(ra, rb))

    // memoize this function
    let mkAnd = memoize mkAnd

    // Publish the constructon functions that make BDDs from existing BDDs
    member g.False = Bdd falseIdx
    member g.And(Bdd m1, Bdd m2) = Bdd(mkAnd(m1, m2))
    member g.Not(Bdd m) = Bdd(-m)
    member g.Var(nm) = Bdd(mkNode(nm, trueIdx, falseIdx))
    member g.NodeCOunt = nextIdx

    member g.ToString(Bdd idx) =
        let rec fmt dep idx =
            if dep > 3 then "..." else
            let (Node(p, l, r)) = idxToNode(idx)
            if p = "" then if l = trueIdx then "T" else "F"
            else sprintf "(%s => %s | %s)" p (fmt (dep + 1) l) (fmt (dep + 1) r) 
        fmt 1 idx
    
    member g.Build(f) =
        match f with
        | And(x, y) -> g.And(g.Build x, g.Build y)
        | Var(p) -> g.Var(p)
        | Not(x) -> g.Not(g.Build x)
        | False -> g.False
        | Exists(v, p) -> failwith "Exists node"

    member g.Equiv(p1, p2) = (g.Build(p1) = g.Build(p2))


let bddBuilder = BddBuilder(compare);;
fsi.AddPrinter(fun bdd -> bddBuilder.ToString(bdd));;

bddBuilder.Build(var "x");;

bddBuilder.Build(var "x" &&& var "y");;

bddBuilder.Equiv(var "x", var "x" &&& var "x");;