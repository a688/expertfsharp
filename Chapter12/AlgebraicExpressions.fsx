﻿
type Expr = 
    | Var
    | Num of int
    | Sum of Expr * Expr
    | Prod of Expr * Expr

let rec deriv expr =
    match expr with
    | Var           -> Num 1
    | Num _         -> Num 0
    | Sum(e1, e2)   -> Sum (deriv e1, deriv e2)
    | Prod(e1, e2)  -> Sum (Prod (e1, deriv e2), Prod (e2, deriv e1))


let simpSum (a, b) = 
    match a, b with
    | Num n, Num m -> Num (n + m)           // constants!
    | Num 0, e | e, Num 0 -> e              // 0+e = e+0 = e
    | e1, e2 -> Sum(e1, e2)

let simpProd (a, b) =
    match a, b with
    | Num n, Num m -> Num (n*m)             // constants!
    | Num 0, e | e, Num 0 -> Num 0          // 0 * e = 0
    | Num 1, e | e, Num 1 -> e              // 1*e = e*1 = e
    | e1, e2 -> Prod(e1, e2)

let rec simpDeriv e =
    match e with
    | Var                   -> Num 1
    | Num _                 -> Num 0
    | Sum (e1, e2)          -> simpSum (simpDeriv e1, simpDeriv e2)
    | Prod (e1, e2)         -> simpSum (simpProd(e1, simpDeriv e2), simpProd (e2, simpDeriv e1))



let e1 = Sum(Num 1, Prod (Num 2, Var));;        // Input is: 1+2x; Output is: 0 + (2*1 + x*0)


let precSum = 10
let precProd = 20

let rec stringOfExpr prec expr =
    match expr with
    | Var -> "x"
    | Num i -> i.ToString()
    | Sum(e1, e2) -> 
        let sum = stringOfExpr precSum e1 + "+" + stringOfExpr precSum e2
        if prec > precSum then "(" + sum + ")"
        else sum
    | Prod(e1, e2) -> stringOfExpr precProd e1 + "*" + stringOfExpr precProd e2

fsi.AddPrinter(fun expr -> stringOfExpr 0 expr);;

let e3 = Prod (Var, Prod (Var, Num 2));;

simpDeriv e3;;