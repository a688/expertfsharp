﻿open System
open Symbolic.Expressions
open Microsoft.FSharp.Text.Lexing

let processOneLine text =
    let lex = LexBuffer<char>.FromString text
    
    let e1 = ExprParser.expr ExprLexer.main lex
    printfn "After parsing: %A" e1

    let e2 = Utils.Simplify e1
    printfn "After simplifying: %A" e2

    let e3 = Utils.diff "x" e2
    printfn "After differentiating: %A" e3

    let e4 = Utils.Simplify e3
    printfn "After simplifying: %A" e4

[<EntryPoint>]
let main argv = 

    while true do
        let text = Console.ReadLine()
        try
            processOneLine text
        with
        | e -> printfn "Error: %A" e

    0 // return an integer exit code
