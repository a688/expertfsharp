﻿// Recursive workflow expressions
let rnd = System.Random()

let rec randomWalk k =
    seq { 
        yield k
        yield! randomWalk (k + rnd.NextDouble())
    }
;;

randomWalk 10.0;;



// using reflection
let intType = typeof<int>;;
intType.FullName;;
intType.AssemblyQualifiedName;;

let intListType = typeof<int list>;;
intListType.FullName;;


