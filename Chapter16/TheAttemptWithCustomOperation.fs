﻿module TheAttemptWithCustomOperation

open TheAttemptBuilder

let gen = new System.Random()
let rand() = gen.NextDouble()

type AttemptBuilder with
    [<CustomOperation("condition", MaintainsVariableSpaceUsingBind = true)>]
    member b.Condition(p, [<ProjectionParameter>] w) = condition p w
    
let attempt = new AttemptBuilder()


let randomNumberInCircle =
    attempt {
        let x, y =  rand(), rand()
        condition (x * x + y * y < 1.0)
        return (x, y)
    }