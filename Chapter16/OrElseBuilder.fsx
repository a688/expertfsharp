﻿
type OrElseBuilder() =
    member this.ReturnFrom(x) = 
        printfn "In ReturnFrom (%A)" x
        x
    member this.Combine (a,b) =
        printfn "in combine, checking if A (%A) or B (%A) has a value or not" a b
        match a with
        | Some _ -> a  // a succeeds -- use it
        | None -> b    // a fails -- use b instead
    member this.Delay(f) = 
        printfn "Delayed"
        f()
    member this.Zero() = 
        printfn "In Zero"
        None

let orElse = new OrElseBuilder()

let map1 = [ ("1","One"); ("2","Two") ] |> Map.ofList
let map2 = [ ("A","Alice"); ("B","Bob"); ("CA", "Cathy") ] |> Map.ofList
let map3 = [ ("CA","California"); ("NY","New York") ] |> Map.ofList

let multiLookup key = orElse {
    return! map1.TryFind key
    return! map2.TryFind key
    return! map3.TryFind key
    }
;;

multiLookup "2" |> printfn "Result for 2 is %A";;
multiLookup "A" |> printfn "Result for A is %A";;
multiLookup "CA" |> printfn "Result for CA is %A";;
multiLookup "NY" |> printfn "Result for NY is %A";;
multiLookup "X" |> printfn "Result for X is %A";;