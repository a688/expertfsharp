﻿open FSharp.Quotations
open FSharp.Quotations.Patterns
open FSharp.Quotations.DerivedPatterns

type Error = Err of float

// Keeps track of a mapping from variables to value/error estimate pairs
let rec errorEstimateAux (e : Expr) (env : Map<Var, _>) =
    match e with
    | SpecificCall <@@ (+) @@> (tyargs, _, [xt; yt]) -> 
        let x, Err(xerr) = errorEstimateAux xt env
        let y, Err(yerr) = errorEstimateAux yt env
        (x + y, Err(xerr + yerr))
    | SpecificCall <@@ (-) @@> (tyargs, _, [xt; yt]) -> 
        let x, Err(xerr) = errorEstimateAux xt env
        let y, Err(yerr) = errorEstimateAux yt env
        (x - y, Err(xerr + yerr))
    | SpecificCall <@@ (*) @@> (tyargs, _, [xt; yt]) -> 
        let x, Err(xerr) = errorEstimateAux xt env
        let y, Err(yerr) = errorEstimateAux yt env
        (x * y, Err(xerr * abs(y) + yerr * abs(x) + xerr * yerr))
    | SpecificCall <@@ abs @@> (tyargs, _, [xt]) ->
        let x, Err(xerr) = errorEstimateAux xt env
        (abs(x), Err(xerr))
    | Let(var, vet, bodyt) ->
        let varv, verr = errorEstimateAux vet env
        errorEstimateAux bodyt (env.Add(var, (varv, verr)))
    | Call(None, MethodWithReflectedDefinition(Lambda(v, body)), [arg]) ->
        errorEstimateAux (Expr.Let(v, arg, body)) env
    | Var(x) -> env.[x]                     // Consult the environment (map) to get this already calculated value
    | Double(n) -> (n, Err(0.0))            // Error is 0 for constants
    | _ -> failwithf "unrecognized term: %A" e

// Checks that the expression given is a lambda expression, using an active pattern. 
let rec errorEstimateRaw (t : Expr) =
    match t with
    | Lambda(x, t) ->
        (fun xv -> errorEstimateAux t (Map.ofSeq [(x, xv)]))
    | PropertyGet(None, PropertyGetterWithReflectedDefinition(body), []) ->
        errorEstimateRaw body
    | _ -> failwithf "unrecognized term: %A - expected a lambda" t

// Converts inputted expression to a raw expression, which is an untyped abstract-syntax representaiton of the expression 
// designed for further processing
let errorEstimate (t : Expr<float -> float>) = errorEstimateRaw t
;;
   

fsi.AddPrinter(fun (x: float, Err v) -> sprintf "%g±%g" x v);;

errorEstimate <@ fun x -> x + 2.0 * x + 3.0 * x * x @> (1.0, Err 0.1);;

errorEstimate <@ fun x -> let y = x + x in y * y + 2.0 @> (1.0, Err 0.1);;


[<ReflectedDefinition>]
let poly x = x + 2.0 * x + 3.0 * (x * x);;

errorEstimate <@ poly @> (3.0, Err 0.1);;

errorEstimate <@ poly @> (30271.3, Err 0.0001);;

[<ReflectedDefinition>]
module Polynomials =
    let poly1 x = x + 2.0 * x + 3.0 * (x * x)
    let poly2 x = x - 3.0 * x + 6.0 * (x * x)
;;

type Errors() =
    static member Estimate([<ReflectedDefinition>] expr) = errorEstimate expr
;;

let est1 = Errors.Estimate(fun x -> x - 3.0 * x + 6.0 * (x * x));;
est1 (3.0, Err 0.1);;
