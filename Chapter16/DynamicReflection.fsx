﻿open System.Reflection

let (?) (obj : obj) (nm : string) : 'T =
    obj.GetType().InvokeMember(nm, BindingFlags.GetProperty, null, obj, [||])
    |> unbox<'T>

let (?<-) (obj : obj) (nm : string) (v : obj) : unit =
    obj.GetType().InvokeMember(nm, BindingFlags.SetProperty, null, obj, [|v|])
    |> ignore
;;

type Record1 = { Length : int; mutable Values : int list};;

let obj1 = box [1;2;3]
let obj2 = box {Length = 4; Values = [3;4;5;7]}

let n1 : int = obj1?Length
let n2 : int = obj2?Length

let valuesOld : int list = obj2?Values
