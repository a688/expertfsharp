﻿module TheAttemptBuilder

type Attempt<'T> = (unit -> 'T option)
let succeed x = (fun () -> Some(x)) : Attempt<'T>       // "lifts" a normal value into a 'typed' value
let fail = (fun () -> None) : Attempt<'T>               // Equivilent of "none", which in this case is actually None
let runAttempt (a : Attempt<'T>) = a()                  // returns itself (identity function)
let bind p rest = 
    match runAttempt p with
    | None -> fail
    | Some r -> (rest r)
let delay f = (fun () -> runAttempt (f()))
let combine p1 p2 = 
    (fun () -> 
        match p1() with 
        | None -> p2() 
        | res -> res
    )
let condition p guard = (fun () ->
    match p() with
    | Some x when guard x -> Some x
    | _ -> None
    )

type AttemptBuilder() =
    // Used to de-sugar uses of 'let!' inside computation expressions
    member b.Bind (p, rest) = bind p rest

    // Delays the construction of an attempmt until just before it is executed
    member b.Delay(f) = delay f

    // Used to de-sugar uses of 'return' inside computation expressions
    member b.Return(x) = succeed x

    // Used to de-sugar uses of 'return!' inside computation expressions
    member b.ReturnFrom(x : Attempt<'T>) = x

    // Used to de-sugar uses of 'c1; c2' inside computation expressions
    member b.Combine(p1 : Attempt<'T>, p2 : Attempt<'T>) = combine p1 p2

    // Used to de-sugar uses of 'if..then..' inside computation expressions
    // when the else branch is empty
    member b.Zero() = fail

let attempt = new AttemptBuilder()

let alwaysOne = attempt { return 1 };;
let alwaysPair = attempt { return (1, "two") };;
runAttempt alwaysOne |> ignore

let failIfBig n = attempt { if n > 1000 then return! fail else return n};;
runAttempt (failIfBig 999) |> ignore
runAttempt (failIfBig 1001) |> ignore

let failIfEitherBig (inp1, inp2) = 
    attempt {
        let! n1 = failIfBig inp1
        let! n2 = failIfBig inp2
        return (n1, n2)
    };;
// Desugars (assign n1 the rsult of 'failIfBig inp1' + pass result to next function + assign n2 to the result of 'failIfBig inp2' + return sum of both values
// attempt.Bind(failIfBig inp1, (fun n1 ->
//    attempt.Bind(failIfBig inp2, (fun n2 -> 
//            let sum = n1 + n2
//            attempt.Return sum))))

let sumIfBothSmall (inp1, inp2) =
    attempt {
        let! n1 = failIfBig inp1
        printfn "Hey, n1 was small!"    // Side effect
        let! n2 = failIfBig inp2
        printfn "n2 was also small!"    // Side effect
        let sum = n1 + n2
        return sum
    }

runAttempt(sumIfBothSmall(999,999)) |> ignore;;   // both prinfns are triggered, returns 1998
runAttempt(sumIfBothSmall(999,1001)) |> ignore;;  // only first printfn is triggered, returns None

let sumIfBothSmall2 (inp1, inp2) =
    attempt {
        let mutable sum = 0
        let! n1 = failIfBig inp1
        sum <- sum + n1                 // Side effect
        let! n2 = failIfBig inp2
        sum <- sum + n2                 // Side effect
        return sum
    }

runAttempt(sumIfBothSmall2(999,999)) |> ignore;;   // sum is 1998
runAttempt(sumIfBothSmall2(999,1001)) |> ignore;;  // returns None

let printThenSeven = 
    attempt {
        printf "starting..."
        return 3 + 4
    }

printThenSeven() |> ignore;;
printThenSeven() |> ignore;;
printThenSeven() |> ignore;;
printThenSeven() |> ignore;; // all of these print "starting...val it : int option = Some 7"

