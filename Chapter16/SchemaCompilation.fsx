﻿open System
open System.IO
open System.Globalization
open FSharp.Reflection

// An attribute to be added to files of a schema record type ot indicate the
// column used in the data format for the schema
type ColumnAttribute(col : int) =
    inherit Attribute()
    member x.Column = col

/// SchemaReader builds an object that automatically transforms lines of text
/// files in comma-seperated form into instances of the given type 'Schema.
/// 'Schema must be an F# record type where each field is attributed with a 
/// ColumnAttribute attribute, thus indicating which column of the data the record
/// field is drawn from. This simple version of the reader understands
/// integer, string, and DateTime values in teh CSV format.
type SchemaReader<'Schema>() =
    // Grab the object for the type that describes the schema
    let schemaType = typeof<'Schema>

    // Grab the fields from that type
    let fields = FSharpType.GetRecordFields(schemaType)

    // For each field find the ColumnAttribute and compute a function
    // to build a value for the field
    let schema = 
        fields
        |> Array.mapi (
            fun fldIdx fld -> 
                let fieldInfo = schemaType.GetProperty(fld.Name)
                let fieldConverter = 
                    match fld.PropertyType with
                    | ty when ty = typeof<string> -> (fun (s : string) -> box s)
                    | ty when ty = typeof<int> -> (System.Int32.Parse >> box)
                    | ty when ty = typeof<DateTime> -> (fun s -> box (DateTime.Parse(s, CultureInfo.InvariantCulture)))
                    | ty -> failwithf "Unknown primitive type %A" ty

                let attrib = 
                    match fieldInfo.GetCustomAttributes(typeof<ColumnAttribute>, false) with
                    | [|(:? ColumnAttribute as attrib)|] -> attrib
                    | _ -> failwithf "No column attribute found on field %s" fld.Name
                
                (fldIdx, fld.Name, attrib.Column, fieldConverter)
        )

    // Compute the permutation defined by the ColumnAttribute indexes
    let columnToFldIdxPermutation c = 
        schema |> Array.pick(fun (fldIdx, _, colIdx, _) -> if colIdx = c then (Some(fldIdx)) else None)

    // Drop the parts of the schema we don't need
    let schema =
        schema |> Array.map (fun (_, fldName, _, fldConv) -> (fldName, fldConv))

    // Compute a function to build instances of the schema type. This uses an 
    // F# library function.
    let objectBuilder = FSharpValue.PreComputeRecordConstructor(schemaType)

    member reader.ReadLine(line : string) =
        let words = line.Split([|','|]) |> Array.map(fun s -> s.Trim())
        if words.Length <> schema.Length then
            failwithf "unexpected number of columns in line %s" line

        let words = words |> Array.permute columnToFldIdxPermutation
        let convertColumn colText (fieldName, fieldConverter) =
            try 
                fieldConverter colText
            with
            | e -> failwithf "error converting '%s' to field '%s'" colText fieldName

        let obj = objectBuilder(Array.map2 convertColumn words schema)

        // Ok, now we know we've dynamically build an object of the right type
        unbox<'Schema>(obj)

    // Now implement a line reader
    member reader.ReadLine (textReader : TextReader) =
        reader.ReadLine(textReader.ReadLine())

    // The reads an entire file
    member reader.ReadFile(file) =
        seq {
            use textReader = File.OpenText(file)
            while not textReader.EndOfStream do
                yield reader.ReadLine(textReader)
        }
;;

type CheeseClub = 
    {
        [<Column(0)>] Name : string
        [<Column(2)>] FavoriteCheese : string
        [<Column(1)>] LastAttendance : System.DateTime
    };;



let filePath = @"C:\Users\jalon\Source\Repos\ExpertFSharp\Chapter16\CheeseClub.txt"
System.IO.File.ReadAllLines(filePath);;

fsi.AddPrinter(fun (c : System.DateTime) -> c.ToString());;
let reader = new SchemaReader<CheeseClub>();;
let recs = reader.ReadFile(filePath);;

recs |> Seq.iter(fun f -> printfn "Name: %s" f.Name);;

