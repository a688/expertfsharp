﻿open FSharp.Data.UnitSystems.SI.UnitNames
open FSharp.Data.UnitSystems.SI.UnitSymbols

[<Measure>]
type click

[<Measure>]
type pixel

[<Measure>]
type money

let rateOfClicks = 200.0<click/s>
let durationOfExecution = 3.5<s>

let numberOfClicks = rateOfClicks * durationOfExecution


let integrateByMidpointRule f (a, b) = (b - a) * f((a + b) / 2.0)
let integrateByTrapezoidalRule f (a, b) = (b - a) * ((f a + f b) / 2.0)
let integrateByIterativeRule f (a, b) n =
    ((b - a) / float n) * ((f a + f b) / 2.0 + List.sum[for k in 1 .. n - 1 -> f (a + float k * (b - a) / float n)])

// Unitized implementations
let integrateByMidpointRuleU (f : float<'u> -> float<'v>) (a : float<'u>, b : float<'u>) = (b - a) * f((a + b) / 2.0)
let integrateByTrapezoidalRuleU (f : float<'u> -> float<'v>) (a : float<'u>, b : float<'u>) = (b - a) * ((f a + f b) / 2.0)
let integrateByIterativeRuleU (f : float<'u> -> float<'v>) (a : float<'u>, b : float<'u>) n =
    ((b - a) / float n) * ((f a + f b) / 2.0 + List.sum[for k in 1 .. n - 1 -> f (a + float k * (b - a) / float n)])


let velocityFunction (t: float<s>) = 100.0<m/s> + t * -9.81<m/s^2>

let distance1 = integrateByMidpointRuleU velocityFunction (0.0<s>, 10.0<s>)
let distance2 = integrateByTrapezoidalRuleU velocityFunction (0.0<s>, 10.0<s>)
let distance3 = integrateByIterativeRuleU velocityFunction (0.0<s>, 10.0<s>) 10


let variance (values : seq<float<_>>) =
    let sqr x = x * x
    let xs = values |> Seq.toArray
    let avg = xs |> Array.average
    let variance = xs |> Array.averageBy (fun x -> sqr (x - avg))
    variance

let standardDeviation values =
    sqrt (variance values)

let rnd = new System.Random()
let rand() = rnd.NextDouble()
let sampleTimes = [for x in 0 .. 1000 -> 50.0<s> + 10.0<s> * rand()]

let exampleDeviation = standardDeviation sampleTimes
let exampleVariance = variance sampleTimes



// type with measure
type Vector2D<[<Measure>] 'u> ( dx : float<'u>, dy : float<'u> ) =
    member v.DX = dx
    member v.DY = dy
    
    /// Get the legnth of the vector
    member v.Length = sqrt(dx * dx + dy * dy)

    /// Get a vector scaled by the given factor
    member v.Scale k = Vector2D(k * dx, k * dy)

    // Return a vector shifted by the given delta in the X coordinate
    member v.ShiftX x = Vector2D(dx + x, dy)

    /// Return a vector shifted by the given delta in the Y coordinate
    member v.ShiftY y = Vector2D(dx, dy + y)

    /// Get the zero vector
    static member Zero = Vector2D<'u>(0.0<_>, 0.0<_>)

    /// Return a constant vector along the X axis
    static member ConstX dx = Vector2D<'u>(dx, 0.0<_>)

    /// Return a constant vector along the Y axis
    static member ConstY dy = Vector2D<'u>(0.0<_>, dy)

    /// Return the sum of two vectors
    static member (+) (v1 : Vector2D<'u>, v2 : Vector2D<'u>) =
        Vector2D(v1.DX + v2.DX, v1.DY + v2.DY)

    /// Return the difference of two vectors
    static member (-) (v1 : Vector2D<'u>, v2 : Vector2D<'u>) =
        Vector2D(v1.DX - v2.DX, v1.DY - v2.DY)

    /// Return the pointwise-product of two vectors
    static member (*) (v1 : Vector2D<'u>, v2 : Vector2D<'u>) =
        Vector2D(v1.DX * v2.DX, v1.DY * v2.DY)