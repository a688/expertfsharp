﻿module deedletest

#load "../packages/Deedle.1.2.4/Deedle.fsx"
#load "../packages/MathNet.Numerics.FSharp.3.10.0/MathNet.Numerics.fsx"

open System
open Deedle
open MathNet.Numerics.Distributions

let start = DateTimeOffset(DateTime.Today)

let randomPrice drift volatility initial (span : TimeSpan) count =
    let dist =
        Normal(0.0, 1.0, RandomSource = Random())
    let dt = span.TotalDays / 250.0
    let driftExp = (drift - 0.5 * pown volatility 2) * dt
    let randExp = volatility * (sqrt dt)
    (start, initial)
    |> Seq.unfold (
        fun (dt, price) -> 
            let price = price * exp (driftExp + randExp * dist.Sample())
            Some((dt, price), (dt + span, price))
        )
    |> Seq.take count

let stock1 = randomPrice 0.1 3.0 20.0 (TimeSpan.FromMinutes(1.0)) 500
let stock2 = randomPrice 0.2 1.5 20.0 (TimeSpan.FromSeconds(30.0)) 1000

let stockSeries1 = series stock1
let stockSeries2 = series stock2

let zippedSeriesWhereBothHaveData = stockSeries1.Zip(stockSeries2, JoinKind.Left)
let zippedSeriesWhereOneHasData = stockSeries1.Zip(stockSeries2, JoinKind.Right)

// Contains value every minute
let f1 = Frame.ofColumns ["S1" => stockSeries1]
// Contains value every 30 seconds
let f2 = Frame.ofColumns ["S2" => stockSeries2]

let alignedData = f1.Join(f2, JoinKind.Outer)

