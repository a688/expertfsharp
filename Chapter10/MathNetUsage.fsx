﻿module MathNetUsage
#load "../packages/MathNet.Numerics.FSharp.3.10.0/MathNet.Numerics.fsx"
#load "../packages/FSharp.Charting.0.90.13/FSharp.Charting.fsx"

open FSharp.Charting
open MathNet.Numerics.Statistics
open MathNet.Numerics.Distributions
open System.Collections.Generic

let data = [ for i in 0.0 .. 0.01 .. 10.0 -> sin i]

let exampleVariance = data |> Statistics.Variance
let exampleMean = data |> Statistics.Mean
let exampleMax = data |> Statistics.Maximum
let exampleMin = data |> Statistics.Minimum

let exampleBellCurve = Normal(100.0, 10.0)
let samples = exampleBellCurve.Samples()

let histogram n data =
    let h = Histogram(data, n)
    [| 
        for i in 0 .. h.BucketCount - 1 
            -> (sprintf "%.0f-%.0f" h.[i].LowerBound) h.[i].UpperBound, h.[i].Count
    |]

samples
|> Seq.truncate 1000
|> histogram 10
|> Chart.Column
