﻿module LATest

#load "../packages/MathNet.Numerics.FSharp.3.10.0/MathNet.Numerics.fsx"

open MathNet.Numerics
open MathNet.Numerics.LinearAlgebra

let rnd = System.Random()
let rand() = rnd.NextDouble()


let largeMatrix = matrix [for i in 1 .. 100 -> [for j in 1 .. 100 -> rand() ]]

let inverse = largeMatrix.Inverse()

let check = largeMatrix * inverse