﻿module mainProg

let variance (values : float[]) =
    let sqr x = x * x
    let avg = values |> Array.average
    let sigma2 = values |> Array.averageBy(fun x -> sqr (x - avg))
    sigma2

let standardDeviation values =
    sqrt (variance values)

// "By" version which allow a consumer to specify a functon to convert
// the instance of one of the objects in the sequence to a float
let varianceBy (f: 'T -> float) values =
    let sqr x = x * x
    let xs = values |> Seq.map f |> Seq.toArray
    let avg = xs |> Array.average
    let res = xs |> Array.averageBy(fun x -> sqr(x - avg))
    res
let standardDeviationBy f values =
    sqrt (varianceBy f values)


// "Generic" versions. Any type passed in vales must support +, -, /, *, and Zero
let inline gvariance values =
    let sqr x = x * x
    let avg = values |> Array.average
    let sigma2 = values |> Array.averageBy(fun x -> sqr (x - avg))
    sigma2

let inline gstandardDeviation values =
    sqrt (gvariance values)
