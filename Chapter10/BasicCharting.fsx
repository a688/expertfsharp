﻿#load "C:\Users\jalon\Source\Repos\ExpertFSharp\packages\FSharp.Charting.0.90.13\FSharp.Charting.fsx"

open FSharp.Charting

let rnd = System.Random()
let rand() = rnd.NextDouble()

let randomPoints = [for i in 0 .. 1000 -> 10.0 * rand(), 10.0 * rand()]

randomPoints
|> Chart.Point

let randomTrend1 = [for i in 0.0 .. 0.1 .. 10.0 -> i, sin i + rand()]
let randomTrend2 = [for i in 0.0 .. 0.1 .. 10.0 -> i, sin i + rand()]

Chart.Combine [Chart.Line randomTrend1; Chart.Line randomTrend2]

Chart.Line (randomTrend1, Title = "Random Trend")

randomPoints
|> fun c -> Chart.Line (c, Title ="Expected Trend")

type RandomPoint = {X : float; Y : float; Z : float}
let random3Dpoints =
    [for i in 1 .. 1000 -> {X = rand(); Y = rand(); Z = rand()}]

let averageX = random3Dpoints |> Seq.averageBy(fun p -> p.X)
let averageY = random3Dpoints |> Seq.averageBy(fun p -> p.Y)
let averageZ = random3Dpoints |> Seq.averageBy(fun p -> p.Z)

let maxX = random3Dpoints |> Seq.maxBy (fun p -> p.Y)

let distanceFromOrigin (p : RandomPoint) =
    sqrt (p.X * p.X + p.Y * p.Y + p.Z * p.Z)

let closest = random3Dpoints |> Seq.minBy distanceFromOrigin

let histogram = 
    random3Dpoints 
    |> Seq.countBy(fun p -> int (p * 10.0 / sqrt 3.0))
    |> Seq.sortBy fst
    |> Seq.toList