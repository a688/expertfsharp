﻿// encode an integer into 1, 2, or 5 bytes
let encode (n : int32) =
    if (n >= 0 && n <= 0x7F) then [n]       // 0x7F is 127
    elif (n >= 0x80 && n <= 0x3FFF) then
        [(0x80 ||| (n >>> 8)) &&& 0xFF; (n &&& 0xFF)]
    else 
        [0xC0;
        ((n >>> 24) &&& 0xFF);
        ((n >>> 16) &&& 0xFF);
        ((n >>> 8) &&& 0xFF);
        (n &&& 0xFF)]

encode 32;;
encode 320;;
encode 32000;;