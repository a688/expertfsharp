﻿open System.Net.Sockets
open System.Net.Security
open System.Security.Authentication
open System.Security.Cryptography.X509Certificates


type AsyncTcpServer(addr, port, handleServerRequest) =
    let socket = new TcpListener(addr, port)

    member x.Start() = async { do x.Run() } |> Async.Start

    member x.Run() = 
        socket.Start()
        while true do
            let client = socket.AcceptTcpClient()
            async {
                try do! handleServerRequest client with e -> ()
                }
            |> Async.Start

type AsyncTcpServerSecure(addr, port, handleServerRequest) =
    // Gets the first certificate with a friendly name of localhost
    let getCertificate() =
        let store = new X509Store(StoreName.My, StoreLocation.LocalMachine)
        store.Open(OpenFlags.ReadOnly)
        let certs =
            store.Certificates.Find(
                findType = X509FindType.FindBySubjectName,
                findValue = System.Net.Dns.GetHostName(),
                validOnly = true)
        certs
        |> Seq.cast<X509Certificate2>
        |> Seq.tryFind(fun c -> c.FriendlyName = "localhost")

    let handleServerRequestSecure (client : TcpClient) =
        async {
            let cert = getCertificate()
            if cert.IsNone then printfn "No cert"; return()
            let stream = client.GetStream();
            let sslStream = new SslStream(innerStream = stream, leaveInnerStreamOpen = true)
            try
                sslStream.AuthenticateAsServer(
                    serverCertificate = cert.Value,
                    clientCertificateRequired = false,
                    enabledSslProtocols = SslProtocols.Default,
                    checkCertificateRevocation = false)
            with _ -> printfn "Can't authenticate"; return()

            printfn "IsAuthenticated: %A" sslStream.IsAuthenticated

            if sslStream.IsAuthenticated then
                /// In this example only the server is authenticated
                printfn "IsEncrypted: %A" sslStream.IsEncrypted
                printfn "IsSigned: %A" sslStream.IsSigned

                // Indicated whether the current side of the connection
                // is authenticated as a server
                printfn "IsServer: %A" sslStream.IsServer

            return! handleServerRequest stream
            }

    let server = AsyncTcpServer(addr, port, handleServerRequestSecure)

    member x.Start() = server.Start()
