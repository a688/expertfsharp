﻿open System.Net
open System.Net.Sockets
open System.IO
open System.Text.RegularExpressions
open System.Text

/// a table of MIME content types
let mimeTypes = 
    dict [
        ".html", "text/html"
        ".htm", "text/html"
        ".txt", "text/plain"
        ".gif", "image/gif"
        ".jpg", "image/jpeg"
        ".png", "image/png"]
   
// Compute a MIME type from a file extension.
let getMimeType(ext) =
    if mimeTypes.ContainsKey(ext) then mimeTypes.[ext]
    else "binary/octet"

// The pattern Regex1 uses a regular expression to match one element
let (|Regex1|_|) (patt : string) (inp : string) =
    try Some(Regex.Match(inp, patt).Groups.Item(1).Captures.Item(0).Value)
    with _ -> None

// The root for the data we serve
let root = @"c:\inetpub\wwwroot"

// Handle a TCP connection for an HTTP Get request
let handleRequest (client: TcpClient) (port : int) =
    async {
        use stream = client.GetStream()
        use out = new StreamWriter(stream)
        let sendHeaders (lines: seq<string>) = 
            let printLine = fprintf out "%s\r\n"
            Seq.iter printLine lines

            // An empty line is required before content, if any.
            printLine ""
            out.Flush()
        let notFound () = sendHeaders["HTTP/1.0 404 Not Found"]

        let inp = new StreamReader(stream)
        let request = inp.ReadLine()

        printfn "Got new request: '%s'" request

        match request with
        // Requests to root are redirected to the start page.
        | "GET / HTTP/1.0" | "GET / HTTP/1.1" -> 
            printfn "Matched: %s" @"GET / HTTP/1.0 or Get / HTTP/1.1"
            sendHeaders [
                "HTTP/1.0 302 Found"
                sprintf "Location: http://localhost:%d/iisstart.htm" port
                ]
        | Regex1 "GET /(.*?) HTTP/1\\.[01]$" fileName ->
            printfn "Matched: %s => %s" @"Get /(.*?) HTTP/1\\.[01]$" fileName
            let fname = Path.Combine(root, fileName)
            let mimeType = getMimeType(Path.GetExtension(fname))
            if not (File.Exists(fname)) then notFound()
            else 
                let content = File.ReadAllBytes fname
                sendHeaders [
                    "HTTP/1.0 200 OK";
                    sprintf "Content-Length: %d" content.Length;
                    sprintf "Content-Type: %s" mimeType
                    ]
                stream.Write(content, 0, content.Length)
        | _ -> 
            printfn "Matched NOTHING"
            notFound()
        }

let server = 
    let port = 8090
    async {
        let socket = new TcpListener(IPAddress.Parse("127.0.0.1"), port)
        socket.Start()
        while true do
            use client = socket.AcceptTcpClient()
            do! handleRequest client port
        }
;;

Async.Start server;;

//http "http://localhost:8090/";;