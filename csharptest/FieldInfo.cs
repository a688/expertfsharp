﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace csharptest
{
    [System.AttributeUsage(System.AttributeTargets.Property | AttributeTargets.Field)]
    public sealed class FieldInfo : System.Attribute
    {
        public int StartPosition { get; set; }

        public int Length { get; set; }

        public string CustomFormat { get; set; }

        public FieldInfo(int startPosition, int length)
        {
            StartPosition = startPosition;
            Length = length;
        }

        public FieldInfo(int startPosition, int length, string customFormat)
        {
            StartPosition = startPosition;
            Length = length;
            CustomFormat = customFormat;
        }
    }
}
