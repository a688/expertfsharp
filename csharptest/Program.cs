﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace csharptest
{
    class Program
    {
        static void Main(string[] args)
        {

            var rec = new AF100();

            var props = rec.GetType().GetProperties();

            foreach(var prop in props)
            {
                System.Console.WriteLine($"Property {prop.Name}");
                var customAttribs = prop.GetCustomAttributes(true);
                foreach(var ca in customAttribs)
                {
                    System.Console.WriteLine($"Attrib: {ca}");
                }
            }

            System.Console.ReadLine();
        }
    }
}
