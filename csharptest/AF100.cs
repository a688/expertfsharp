﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace csharptest
{
    public sealed class AF100
    {
        [FieldInfo(0, 8)]
        public string Cusip { get; set; }

        public string Description { get; set; }

        public double CurrentFace { get; set; }
    }
}
