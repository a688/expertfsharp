﻿[<System.Diagnostics.DebuggerDisplay("{re}+{im}i")>]
type MyComplex = {re : double; im : double}

let c = {re = 0.0; im = 0.0}
System.Console.WriteLine("{0}+{1}i", c.re, c.im)