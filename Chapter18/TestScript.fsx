﻿#if INTERACTIVE
#r "bin/debug/Chapter18.dll"
#r "../packages/NUnit.3.6.0/lib/net45/nunit.framework.dll"
#else
module IsPalindromeTests
#endif
;;

open System
open NUnit.Framework
open IsPalindrome
;;
[<Test>]
let ``isPalindrome return true on the empty string`` () =
    Assert.That(isPalindrome(""), Is.True, "IsPalindrome must return true on an empty string")
;;
``isPalindrome return true on the empty string``();;


