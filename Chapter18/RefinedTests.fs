﻿namespace MoreTests

open System
open NUnit.Framework
open FsCheck
open FsCheck.NUnit
open IsPalindrome

[<TestFixture; Description("Test fix for the isPalindrome function")>]
type Test() =
    [<OneTimeSetUpAttribute>]
    member x.InitTestFixture () =
        printfn "Before running Fixture"

    [<OneTimeTearDownAttribute>]
    member x.DoneTestFixture () =
        printfn "After running Fixture"

    [<SetUp>]
    member x.InitTest () =
        printfn "Before running test"

    [<TearDown>]
    member x.DoneTest () =
        printfn "After funning test"

    [<Test; Category("Special Case"); Description("An empty string is palindrome")>]
    member x.EmptyString() =
        Assert.That(isPalindrome(""), Is.True, "isPalindrome must return true on an empty string")


    [<Property>]
    member x.RevTwiceTest() =
        revTwice

    [<Property>]
    member x.RevOnceTest() =
        revOnce

    