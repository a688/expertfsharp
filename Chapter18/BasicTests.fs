﻿module Main

open System
open NUnit.Framework
open IsPalindrome


let posTests xs = 
    for s in xs do
        Assert.That(isPalindrome s, Is.True, sprintf "isPalindrome(\"%s\") must return true" s)

let negTests xs = 
    for s in xs do
        Assert.That(isPalindrome s, Is.False, sprintf "isPalindrome(\"%s\") must return false" s)
    
[<Test>]
let ``isPalindrome returns true on the empty string`` () =
    Assert.That(isPalindrome(""), Is.True, "isPalindrome must return true on an empty string")

[<Test>]
let ``isPalindrome returns true for a single character`` () =
    posTests ["a"]

[<Test>]
let ``isPalindrome returns true for even examples`` () =
    posTests ["aa"; "abba"; "abaaba"]

[<Test>]
let ``isPalindrome returns true for odd examples`` () =
    posTests ["aba"; "abbba"; "abababa"]

[<Test>]
let ``isPalindrome returns false for some examples`` () =
    negTests ["as"; "F# is wonderful"; "Nice"]

[<EntryPoint>]
let main argv = 
    

    0 // return an integer exit code
