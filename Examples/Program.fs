﻿// Learn more about F# at http://fsharp.org
// See the 'F# Tutorial' project for more help.

[<EntryPoint>]
let main argv = 
    let simpleSeq = seq { for x in 1 .. 5 -> x }

    let xTimes2 = 
        simpleSeq
        |> Seq.map(fun x -> sprintf "x * 2 = %i" (x * 2) )

    printfn "simpleArray: %A" simpleSeq
    printfn "xTimes2: %A" xTimes2
   

    
    0 // return an integer exit code