﻿open System
open System.ComponentModel
open Eto
open Eto.Forms
open Eto.Drawing
open EtoUtils

type CoordinateSystem =
    | World
    | View

type TransformableViewControl() as this =
    inherit Drawable()

    let w2v = Matrix.Create()
    let v2w = Matrix.Create()
    let viewpaintEvt = new Event<PaintEventArgs>()
    let worldpaintEvt = new Event<PaintEventArgs>()

    do this.CanFocus <- true

    member this.Translate (v : CoordinateSystem) (p : PointF) = 
        let am, bm = match v with World -> w2v, v2w | View -> v2w, w2v
        am.Translate(p)
        bm.Append(Matrix.FromTranslation(-p.X, -p.Y))

    member this.Rotate (v : CoordinateSystem) (a : single) =
        let am, bm = match v with World -> w2v, v2w | View -> v2w, w2v
        am.Rotate(a)
        bm.Append(Matrix.FromRotation(-a))

    member this.Scale (v : CoordinateSystem) (s : SizeF) =
        let am, bm = match v with World -> w2v, v2w | View -> v2w, w2v
        am.Scale(s)
        bm.Append(Matrix.FromScale(1.f / s.Width, 1.f / s.Height))

    member this.RotateAt (v : CoordinateSystem) (a : single, p : PointF) =
        let am, bm = match v with World -> w2v, v2w | View -> v2w, w2v
        am.RotateAt(a, p)
        bm.Append(Matrix.FromRotationAt(-a, p))

    member this.ScaleAt (v: CoordinateSystem) (s : SizeF, p : PointF) = 
        let am, bm = match v with World -> w2v, v2w | View -> v2w, w2v
        am.ScaleAt(s, p)
        bm.Append(Matrix.FromScaleAt(1.f / s.Width, 1.f / s.Height, p.X, p.Y))

    member this.View2WorldMatrix = v2w
    member this.World2ViewMatrix = w2v

    member this.WorldPaint = worldpaintEvt.Publish
    member this.ViewPaint = viewpaintEvt.Publish

    abstract OnViewPaint : e:PaintEventArgs -> unit
    default this.OnViewPaint _ = ()
    abstract OnWorldPaint : e : PaintEventArgs -> unit
    default this.OnWorldPaint _ = ()

    override this.OnPaint e =
        let g = e.Graphics
        let wr = e.ClipRectangle |> v2w.TransformRectangle

        g.SaveTransform()
        g.MultiplyTransform(w2v)
        let wpe = PaintEventArgs(g, wr)
        this.OnWorldPaint(wpe)
        worldpaintEvt.Trigger(wpe)
        g.RestoreTransform()

        this.OnViewPaint(e)
        viewpaintEvt.Trigger(e)

        base.OnPaint e

    override this.OnKeyDown e =
        let translateV = this.Translate View
        let rotateAtV = this.RotateAt View
        let scaleAtV = this.ScaleAt View
        let mid = PointF(single(this.ClientSize.Width) / 2.f, single(this.ClientSize.Height) / 2.f)
        match e.Key with
        | Keys.W -> translateV(PointF(0.f, -10.f)); this.Invalidate()
        | Keys.A -> translateV(PointF(-10.f, 0.f)); this.Invalidate()
        | Keys.S -> translateV(PointF(0.f, 10.f)); this.Invalidate()
        | Keys.D -> translateV(PointF(10.f, 0.f)); this.Invalidate()
        | Keys.Q -> rotateAtV(10.f, mid); this.Invalidate()
        | Keys.E -> rotateAtV(-10.f, mid); this.Invalidate()
        | Keys.Z -> scaleAtV(SizeF(1.1f, 1.1f), mid); this.Invalidate()
        | Keys.X -> scaleAtV(SizeF(1.f/1.1f, 1.f/1.1f), mid); this.Invalidate()
        | _ -> ()

type LightWeightControl() =
    let mutable coordinateSystem = World
    let mutable location = PointF()
    let mutable size = SizeF()
    let mutable parent : LightWeightContainerControl option = None

    member this.CoordinateSystem
        with get() = coordinateSystem
        and set(v) = coordinateSystem <- v

    member this.Location
        with get() = location
        and set(v) = location <-v; this.Invalidate()

    member this.Size
        with get() = size
        and set(v) = size <- v; this.OnSizeChanged(new System.EventArgs())

    member this.Bounds = RectangleF(location, size)

    member this.Parent
        with get() = parent
        and set(v) = parent <- v

    member this.Invalidate() =
        match parent with
        | Some v -> v.Invalidate(Rectangle(this.Bounds))
        | None -> ()

    member this.Invalidate (r : RectangleF) =
        match parent with
        | Some v ->
            let tr = r
            tr.Offset(location)
            if coordinateSystem = World then
                v.Invalidate(tr |> v.World2ViewMatrix.TransformRectangle |> Rectangle)
            else
                v.Invalidate(tr |> Rectangle)
        | None -> ()

    member this.Focus() =
        match parent with
        | Some p -> p.SetFocus(this)
        | None -> ()

    member this.Unfocus() =
        match parent with
        | Some p -> p.UnsetFocus()
        | None -> ()

    abstract OnPaint : PaintEventArgs -> unit
    default this.OnPaint _ = ()
    abstract OnMouseDown : MouseEventArgs -> unit
    default this.OnMouseDown _ = ()
    abstract OnMouseUp : MouseEventArgs -> unit
    default this.OnMouseUp _ = ()
    abstract OnMouseMove : MouseEventArgs -> unit
    default this.OnMouseMove _ = ()
    abstract OnMouseEnter : MouseEventArgs -> unit
    default this.OnMouseEnter _ = ()
    abstract OnMouseLeave : MouseEventArgs -> unit
    default this.OnMouseLeave _ = ()
    abstract OnMouseDoubleClick : MouseEventArgs -> unit
    default this.OnMouseDoubleClick _ = ()
    abstract OnMouseWheel : MouseEventArgs -> unit
    default this.OnMouseWheel _ = ()
    abstract OnKeyDown : KeyEventArgs -> unit
    default this.OnKeyDown _ = ()
    abstract OnKeyUp : KeyEventArgs -> unit
    default this.OnKeyUp _ = ()
    abstract OnSizeChanged : System.EventArgs -> unit
    default this.OnSizeChanged _ = ()
    abstract HitTest : PointF -> bool
    default this.HitTest p = RectangleF(PointF(), size).Contains(p)

and LightWeightContainerControl() as this =
    inherit TransformableViewControl()

    let children = new ResizeArray<LightWeightControl>()
    let mutable gotFocus : LightWeightControl option = None
    let controlsIn view = children |> Seq.filter (fun v -> v.CoordinateSystem = view)
    let mutable captured : LightWeightControl option = None

    let correlate (loc : PointF) =
        let cv = controlsIn View |> Seq.tryFindBack (fun v -> v.HitTest(PointF(loc.X - v.Location.X, loc.Y - v.Location.Y)))
        match cv with
        | Some c -> Some c
        | None ->
            let l = this.View2WorldMatrix.TransformPoint(loc)
            controlsIn World
            |> Seq.tryFindBack (fun v -> v.HitTest(PointF(l.X - v.Location.X, l.Y - v.Location.Y)))
        
    let cloneMouseEvent (e : MouseEventArgs) (newloc : PointF) =
        new MouseEventArgs(e.Buttons, e.Modifiers, newloc, new System.Nullable<SizeF>(e.Delta), e.Pressure)

    let lightweightCoord (c : LightWeightControl) (loc : PointF) =
        let l = if c.CoordinateSystem = World then this.View2WorldMatrix.TransformPoint(loc) else loc
        PointF(l.X - c.Location.X, l.Y - c.Location.Y)

    let handleMouseEvent (e : MouseEventArgs) (f : LightWeightControl -> MouseEventArgs -> unit) =
        let cv = correlate e.Location
        match cv with
        | Some c -> f c (cloneMouseEvent e (lightweightCoord c e.Location))
        | None -> ()

    let mutable lastControl : LightWeightControl option = None

    member this.AddControl (c : LightWeightControl) = children.Add(c)
    member this.RemoveControl (c : LightWeightControl) = children.Remove(c)
    member this.BringToFront (c : LightWeightControl) = if children.Remove(c) then children.Add(c)
    member this.SendToBack (c : LightWeightControl) = if children.Remove(c) then children.Insert(0, c)
    member this.SetFocus (c : LightWeightControl) = gotFocus <- Some c
    member this.UnsetFocus() = gotFocus <- None

    override this.OnMouseDown e =
        handleMouseEvent e (fun c e -> captured <- Some c; c.OnMouseDown e)
        base.OnMouseDown(e)

    override this.OnMouseUp e =
        match captured with
        | Some c ->
            c.OnMouseUp (cloneMouseEvent e (lightweightCoord c e.Location))
            captured <- None
        | None -> ()
        handleMouseEvent e (fun c e -> c.OnMouseUp e)
        base.OnMouseUp(e)

    override this.OnMouseMove e =
        let lc = lastControl
        let mutable ce = null
        lastControl <- None
        match captured with
        | Some c -> c.OnMouseMove (cloneMouseEvent e (lightweightCoord c e.Location))
        | None -> ()
        handleMouseEvent e (fun c e -> lastControl <- Some(c); ce <- e; c.OnMouseMove e)
        if lastControl <> lc then
            if lastControl = None then lc.Value.OnMouseLeave(ce)
            else lastControl.Value.OnMouseEnter(ce)
        base.OnMouseMove(e)

    override this.OnMouseDoubleClick e =
        handleMouseEvent e (fun c e -> c.OnMouseDoubleClick e)
        base.OnMouseDoubleClick(e)

    override this.OnMouseWheel e =
        handleMouseEvent e (fun c e -> c.OnMouseWheel e)
        base.OnMouseWheel(e)

    override this.OnKeyDown e =
        match gotFocus with
        | Some c -> c.OnKeyDown e
        | None -> ()

    override this.OnKeyUp e =
        match gotFocus with
        | Some c -> c.OnKeyUp e
        | None -> ()

    override this.OnViewPaint e =
        for v in controlsIn View do
            e.Graphics.SaveTransform()
            e.Graphics.TranslateTransform(v.Location)
            v.OnPaint(e)
            e.Graphics.RestoreTransform()

    override this.OnWorldPaint e =
        for v in controlsIn World do
            e.Graphics.SaveTransform()
            e.Graphics.TranslateTransform(v.Location)
            v.OnPaint(e)
            e.Graphics.RestoreTransform()


type Handle() =
    inherit LightWeightControl(CoordinateSystem = World, Size = SizeF(10.f, 10.f))

    let mutable off : PointF option = None
    
    override this.OnMouseDown e =
        off <- Some(e.Location)
    
    override this.OnMouseMove e =
        match off with
        | Some p ->
            this.Location <- PointF(e.Location.X + this.Location.X - p.X, e.Location.Y + this.Location.Y - p.Y)
        | None -> ()

    override this.OnMouseUp _ =
        off <- None

    override this.OnPaint e =
        let g = e.Graphics
        let p = PointF(-(this.Size.Width / 2.f), -(this.Size.Height / 2.f))
        g.DrawEllipse(Colors.Black, RectangleF(p, this.Size))

    override this.HitTest p =
        let xa, yb = p.X / this.Size.Width, p.Y / this.Size.Height
        xa * xa + yb * yb <= 1.f

type Cross() =
    inherit LightWeightControl(CoordinateSystem = World, Size = SizeF(20.f, 20.f))

    override this.OnPaint e =
        let g = e.Graphics
        g.DrawLine(Colors.Black, -10.f, 0.f, 10.f, 0.f)
        g.DrawLine(Colors.Black, 0.f, -10.f, 0.f, 10.f)

type Line() as this =
    inherit LightWeightContainerControl()

    let handles = 
        [|
            Handle(Parent = Some(this :> LightWeightContainerControl), Location = PointF(0.f, 0.f));
            Handle(Parent = Some(this :> LightWeightContainerControl), Location = PointF(100.f, 100.f))
        |]

    let cross = new Cross()

    do
        for h in handles do this.AddControl(h)
        this.AddControl(cross)

    override this.OnSizeChanged _ =
        cross.Location <- PointF(single(this.Width / 2), single(this.Height / 2))

    override this.OnWorldPaint e =
        let g = e.Graphics
        let h1, h2 = handles.[0].Location, handles.[1].Location
        g.DrawLine(Colors.Red, h1, h2)
        base.OnWorldPaint(e)


[<System.STAThread>]
[<EntryPoint>]
let main argv = 
    try
        let app = new Application()
        let form = new Form(Title = "Hello world Eto Forms", Topmost = true, Size = Size(640, 480))
        let my = new Line()
        form.Content <- my
        form.Show()
        app.Run(form)
    with
    | ex -> printfn "%s" ex.Message

    0 // return an integer exit code
