﻿module Views

open System
open System.ComponentModel
open Eto
open Eto.Forms
open Eto.Drawing
open EtoUtils

type CoordinateSystem =
    | World
    | View

type TransformableViewControl() as this =
    inherit Drawable()

    let w2v = Matrix.Create()
    let v2w = Matrix.Create()
    let viewpaintEvt = new Event<PaintEventArgs>()
    let worldpaintEvt = new Event<PaintEventArgs>()

    do this.CanFocus <- true

    member this.Translate (v : CoordinateSystem) (p : PointF) = 
        let am, bm = match v with World -> w2v, v2w | View -> v2w, w2v
        am.Translate(p)
        bm.Append(Matrix.FromTranslation(-p.X, -p.Y))

    member this.Rotate (v : CoordinateSystem) (a : single) =
        let am, bm = match v with World -> w2v, v2w | View -> v2w, w2v
        am.Rotate(a)
        bm.Append(Matrix.FromRotation(-a))

    member this.Scale (v : CoordinateSystem) (s : SizeF) =
        let am, bm = match v with World -> w2v, v2w | View -> v2w, w2v
        am.Scale(s)
        bm.Append(Matrix.FromScale(1.f / s.Width, 1.f / s.Height))

    member this.RotateAt (v : CoordinateSystem) (a : single, p : PointF) =
        let am, bm = match v with World -> w2v, v2w | View -> v2w, w2v
        am.RotateAt(a, p)
        bm.Append(Matrix.FromRotationAt(-a, p))

    member this.ScaleAt (v: CoordinateSystem) (s : SizeF, p : PointF) = 
        let am, bm = match v with World -> w2v, v2w | View -> v2w, w2v
        am.ScaleAt(s, p)
        bm.Append(Matrix.FromScaleAt(1.f / s.Width, 1.f / s.Height, p.X, p.Y))

    member this.View2WorldMatrix = v2w
    member this.World2ViewMatrix = w2v

    member this.WorldPaint = worldpaintEvt.Publish
    member this.ViewPaint = viewpaintEvt.Publish

    abstract OnViewPaint : e:PaintEventArgs -> unit
    default this.OnViewPaint _ = ()
    abstract OnWorldPaint : e : PaintEventArgs -> unit
    default this.OnWorldPaint _ = ()

    override this.OnPaint e =
        let g = e.Graphics
        let wr = e.ClipRectangle |> v2w.TransformRectangle

        g.SaveTransform()
        g.MultiplyTransform(w2v)
        let wpe = PaintEventArgs(g, wr)
        this.OnWorldPaint(wpe)
        worldpaintEvt.Trigger(wpe)
        g.RestoreTransform()

        this.OnViewPaint(e)
        viewpaintEvt.Trigger(e)

        base.OnPaint e

    override this.OnKeyDown e =
        let translateV = this.Translate View
        let rotateAtV = this.RotateAt View
        let scaleAtV = this.ScaleAt View
        let mid = PointF(single(this.ClientSize.Width) / 2.f, single(this.ClientSize.Height) / 2.f)
        match e.Key with
        | Keys.W -> translateV(PointF(0.f, -10.f)); this.Invalidate()
        | Keys.A -> translateV(PointF(-10.f, 0.f)); this.Invalidate()
        | Keys.S -> translateV(PointF(0.f, 10.f)); this.Invalidate()
        | Keys.D -> translateV(PointF(10.f, 0.f)); this.Invalidate()
        | Keys.Q -> rotateAtV(10.f, mid); this.Invalidate()
        | Keys.E -> rotateAtV(-10.f, mid); this.Invalidate()
        | Keys.Z -> scaleAtV(SizeF(1.1f, 1.1f), mid); this.Invalidate()
        | Keys.X -> scaleAtV(SizeF(1.f/1.1f, 1.f/1.1f), mid); this.Invalidate()
        | _ -> ()

[<System.STAThread>]
//[<EntryPoint>]
let main argv = 
    try
        let app = new Application()
        let form = new Form(Title = "Views", Topmost = true, Size = Size(640, 480))
        let draw = new TransformableViewControl()        
    
        let cpt = [| PointF(20.f, 60.f); PointF(40.f, 50.f); PointF(130.f, 60.f); PointF(200.f, 200.f) |]
        let mutable movingPoint = -1

        let menuBezier = new CheckMenuItem(Text = "Show &Bezier", Checked = true)
        let menuCanonical = new CheckMenuItem(Text = "Show &Canonical spline")
        let menuControlPoints = new CheckMenuItem(Text = "Show control &points")

        let tension = new Slider(Orientation = Orientation.Vertical, MinValue = 0, MaxValue = 10, TickFrequency = 1, Visible = false)

        let drawPoint (g : Graphics) (p : PointF) =
            g.DrawEllipse(Pens.Red, p.X - 2.f, p.Y - 2.f, 4.f, 4.f)

        let paint (g: Graphics) =
            if (menuBezier.Checked) then
                g.DrawLine(Pens.Red, cpt.[0], cpt.[1])
                g.DrawLine(Pens.Red, cpt.[2], cpt.[3])
                let path = new GraphicsPath()
                path.AddBezier(cpt.[0], cpt.[1], cpt.[2], cpt.[3])
                g.DrawPath(Pens.Black, path)
            if (menuCanonical.Checked) then
                let path = new GraphicsPath()
                path.AddCurve(cpt, single tension.Value)
                g.DrawPath(Pens.Blue, path)
            if (menuControlPoints.Checked) then
                for i = 0 to cpt.Length - 1 do
                    drawPoint g cpt.[i]
    
        let isClose (p : PointF) (l : PointF) =
            let dx = p.X - l.X
            let dy = p.Y - l.Y
            (dx * dx + dy * dy) < 6.f

        let mouseDown (p : PointF) =
            match cpt |> Array.tryFindIndex(isClose p) with
            | Some idx -> movingPoint <- idx
            | None -> ()

        let mouseMove (p : PointF) = 
            if (movingPoint <> -1) then
                cpt.[movingPoint] <- p
                draw.Invalidate()

        let updatemenu _ = draw.Invalidate()

        let menu = new MenuBar()
        let menuFile = SubMenu("&File", [ ActionMenuItem("E&xit").WithAction(fun _ -> app.Quit()) ]) |> makeMenu
        let menuSettings = 
            SubMenu("&Settings", 
                [
                    Item(menuBezier).WithAction(updatemenu)
                    Item(menuCanonical).WithAction(
                        fun _ -> 
                            draw.Invalidate()
                            tension.Visible <- menuCanonical.Checked)
                    Item(menuControlPoints).WithAction(updatemenu)
                ]
            ) 
            |> makeMenu

        [ menuFile; menuSettings] |> List.iter(fun m -> menu.Items.Add(m))
        form.Menu <- menu

        tension.ValueChanged.Add(fun _ -> draw.Invalidate())
        draw.Paint.Add(fun e -> paint e.Graphics)
        draw.MouseDown.Add(fun e -> mouseDown(e.Location))
        draw.MouseMove.Add(fun e -> mouseMove(e.Location))
        draw.MouseUp.Add(fun e -> movingPoint <- -1)

        let l = Tbl [ StretchedRow[ StretchedEl(draw); El(tension) ] ] |> makeLayout

        form.Content <- l

        draw.WorldPaint.Add(fun e -> paint e.Graphics) |> ignore

        draw.MouseDown.Add(fun e ->
            let we = draw.View2WorldMatrix.TransformPoint(e.Location)
            mouseDown(we)
            )

        draw.MouseUp.Add(fun e ->
            let we = draw.View2WorldMatrix.TransformPoint(e.Location)
            mouseMove(we)
            )

        form.Content <- l
        form.Show()
        app.Run(form)
    with
    | ex -> printfn "%s" ex.Message

    0 // return an integer exit code
