﻿module Mandelbrot

open Eto
open Eto.Forms
open Eto.Drawing
open EtoUtils
open System.Numerics

let sqrMod (x : Complex) = x.Real * x.Real + x.Imaginary * x.Imaginary

let rec mandel maxit (z : Complex) (c : Complex) count =
    if (sqrMod(z) < 4.0) && (count < maxit) then
        mandel maxit ((z * z) + c) c (count + 1)
    else count

let RGBtoHSV (r, g, b) =
    let (m : float) = min r (min g b)
    let (M: float) = max r (max g b)
    let delta = M - m
    let posh (h : float) = if h < 0.0 then h + 360.0 else h
    let deltaf (f : float) (s : float) = (f - s) / delta
    if M = 0.0 then 
        (-1.0, 0.0, M)
    else
        let s = (M - m) / M
        if r = M then (posh(60.0 * (deltaf g b)), s, M)
        elif g = M then (posh(60.0 * (2.0 + (deltaf b r ))), s, M)
        else (posh(60.0 * (4.0 + (deltaf r g))), s, M)

let HSVtoRGB (h, s, v) =
    if s = 0.0 then (v, v, v)
    else
        let hs = h / 60.0
        let i = floor (hs)
        let f = hs - i
        let p = v * (1.0 - s)
        let q = v * (1.0 - s * f)
        let t = v * (1.0 - s * (1.0 - f))
        match int i with
        | 0 -> (v, t, p)
        | 1 -> (q, v, p)
        | 2 -> (p, v, t)
        | 3 -> (p, q, v)
        | 4 -> (t, p, v)
        | _ -> (v, p, q)

let makeColor (r, g, b) =
    let f x = int32(x * 255.0)
    Color.FromArgb(f(r), f(g), f(b))

let defaultColor i = 
    makeColor(HSVtoRGB(360.0 * (float i / 250.0), 1.0, 1.0))

let coloring =
    [|
        defaultColor;
        (fun i -> Color.FromArgb(i, i, i));
        (fun i -> Color.FromArgb(i, 0, 0));
        (fun i -> Color.FromArgb(0, i, 0));
        (fun i -> Color.FromArgb(0, 0, i));
        (fun i -> Color.FromArgb(i, i, 0));
        (fun i -> Color.FromArgb(i, 250 - i, 0));
        (fun i -> Color.FromArgb(250 - i, i, i));
        (fun i -> if i % 2 = 0 then Color.FromArgb(255, 255, 255, 255) else Color.FromArgb(0, 0, 0, 255));
        (fun i -> Color.FromArgb(250 - i, 250 - i, 250 - i))
    |]

let createPalette c = 
    Array.init 253 (function 
        | 250 -> Colors.Black
        | 251 -> Colors.White
        | 252 -> Colors.LightGrey
        | i -> c i)

let mutable palette = createPalette coloring.[0]

let pickColor maxit it = 
    palette.[int(250.0 * float it / float maxit)]

let clearOffScreen (b : Bitmap) = 
    use g = new Graphics(b)
    g.Clear(Brushes.White)

[<System.STAThread>]
//[<EntryPoint>]
let main argv = 
    try
        let app = new Application()
        let form = new Form(Title = "Mandelbrot set", Topmost = true, Size = Size(800, 600))
        let draw = new Drawable()

        let timer = new UITimer(Interval = 0.1)
        timer.Elapsed.Add(fun _ -> draw.Invalidate())

        let mutable worker = System.Threading.Thread.CurrentThread
        let mutable bitmap = new Bitmap(form.Width, form.Height, PixelFormat.Format32bppRgba)
        let mutable bmpw = form.Width
        let mutable bmph = form.Height
        let mutable rect = RectangleF.Empty
        let mutable tl = (-3.0, 2.0)
        let mutable br = (2.0, -2.0)
        let clipboard = new Clipboard()

        let mutable menuIterations = 150

        let iterations (tlx, tly) (brx, bry) =
            menuIterations

        let paint (g : Graphics) =
            lock bitmap (fun () -> g.DrawImage(bitmap, 0.f, 0.f))
            g.DrawRectangle(Pens.Black, rect)
            use bg = new SolidBrush(Color.FromArgb(0x80FFFFFF))
            g.FillRectangle(bg, rect)

        // t = top, l = left, r = right, b = bottom, bm = bitmap, p = pixel, w = width, h = height
        let run filler (form : #Form) (bitmap : Bitmap) (tlx, tly) (brx, bry) =
            let dx = (brx - tlx) / float bmpw
            let dy = (tly - bry) / float bmph
            let maxit = iterations (tlx, tly) (brx, bry)
            let x = 0
            let y = 0
            let transform x y = new Complex(tlx + (float x) * dx, tly - (float y) * dy)
            app.AsyncInvoke(fun _ -> form.Title <- sprintf "Mandelbrot set [it: %d] (%f, %f) -> (%f %f)" maxit tlx tly brx bry)
            filler maxit transform
            timer.Stop()

        let linearFill (bw : int) (bh : int) maxit map =
            for y = 0 to bh - 1 do
                for x = 0 to bw - 1 do
                    let c = mandel maxit Complex.Zero (map x y) 0
                    lock bitmap (fun() -> bitmap.SetPixel(x, y, pickColor maxit c))

        let blockFill (bw : int) (bh : int) maxit map =
            let rec fillBlock first sz x y =
                if x < bw then
                    let c = mandel maxit Complex.Zero (map x y) 0
                    lock bitmap (fun() -> 
                        use g = new Graphics(bitmap)
                        use b = new SolidBrush(pickColor maxit c)
                        g.FillRectangle(b, single x, single y, single  sz, single sz)
                        )
                    fillBlock first sz (if first || ((y / sz) % 2 = 1) then x + sz else x + 2 * sz) y
                elif sz > 1 then
                    fillBlock false (sz / 2) (sz / 2) 0

            fillBlock true 64 0 0

        let mutable fillFun = blockFill

        let stopWorker() =
            if worker <> System.Threading.Thread.CurrentThread then
                worker.Abort()
                worker <- System.Threading.Thread.CurrentThread

        let drawMandel () =
            let bf = fillFun bmpw bmph
            stopWorker()
            timer.Start()
            worker <- new System.Threading.Thread(fun () -> run bf form bitmap tl br)
            worker.IsBackground <- true
            worker.Priority <- System.Threading.ThreadPriority.Lowest
            worker.Start()

    
        let mutable startsel = PointF.Empty

        let setCoord (tlx : float, tly : float) (brx : float, bry : float) =
            let dx = (brx - tlx) / float bmpw
            let dy = (tly - bry) / float bmph
            let mapx x = tlx + float x * dx
            let mapy y = tly - float y * dy
            tl <- (mapx rect.Left, mapy rect.Top)
            br <- (mapx rect.Right, mapy rect.Bottom)

        let ensureAspectRatio (tlx : float, tly : float) (brx : float, bry : float) =
            let ratio = (float bmpw / float bmph)
            let w, h = abs(brx - tlx), abs(tly - bry)
            if ratio * h > w then
                br <- (tlx + h * ratio, bry)
            else
                br <- (brx, tly - w / ratio)

        let updateView() =
            if rect <> RectangleF.Empty then setCoord tl br
            ensureAspectRatio tl br
            rect <- RectangleF.Empty
            stopWorker()
            clearOffScreen bitmap
            drawMandel()

        let click (arg : MouseEventArgs) =
            if rect.Contains(arg.Location) then
                updateView()
            else
                draw.Invalidate()
                rect <- RectangleF.Empty 
                startsel <- arg.Location

        let mouseMove (arg : MouseEventArgs) =  
            if arg.Buttons = MouseButtons.Primary then
                let tlx = min startsel.X arg.Location.X
                let tly = min startsel.Y arg.Location.Y
                let brx = max startsel.X arg.Location.X
                let bry = max startsel.Y arg.Location.Y
                rect <- new RectangleF(tlx, tly, brx - tlx, bry - tly)
                draw.Invalidate()

        let resize() =
            if bmpw <> form.ClientSize.Width || bmph <> form.ClientSize.Height then
                stopWorker()
                rect <- new RectangleF(SizeF(single form.ClientSize.Width, single form.ClientSize.Height))
                bitmap <- new Bitmap(form.ClientSize.Width, form.ClientSize.Height, PixelFormat.Format32bppRgba)
                bmpw <- form.ClientSize.Width
                bmph <- form.ClientSize.Height
                
                updateView()

        let zoom amount (tlx, tly) (brx, bry) =
            let w, h = abs(brx - tlx), abs(tly - bry)
            let nw, nh = amount * w, amount * h
            tl <- (tlx + (w - nw) / 2., tly - (h - nh) / 2.)
            br <- (brx - (w - nw) / 2., bry + (h - nh) / 2.)
            rect <- RectangleF.Empty
            updateView()

        let selectDropDownItem (l : list<MenuItem>) (o : CheckMenuItem) =
            for el in l do
                let item = (el :?> CheckMenuItem)
                item.Checked <- (o = item)

        let setFillMode (p : list<MenuItem>) (m : CheckMenuItem) filler _ =
            if (not m.Checked) then
                selectDropDownItem p m
                fillFun <- filler
                drawMandel()

        let setupMenu() =
            let m = new MenuBar()
            let setFillMode filler = 
                fillFun <- filler
                drawMandel()

            let itchg = fun (m: MenuItem) ->
                menuIterations <- System.Int32.Parse(m.Text)
                stopWorker()
                drawMandel()

            let setPalette idx =
                palette <- createPalette coloring.[idx]
                stopWorker()
                drawMandel()

            let f = SubMenu ("&File", [ ActionMenuItem("E&xit").WithAction(fun _ -> app.Quit()) ])

            let c = 
                SubMenu 
                    ("&Settings", 
                        [
                            SubMenu
                                ("Color Scheme",
                                    [
                                        RadioMenuItem("colscheme", "HSL Color").WithCheck().WithAction(fun _ -> setPalette 0)
                                        RadioMenuItem("colorscheme", "Gray").WithAction(fun _ -> setPalette 1)
                                        RadioMenuItem("colorscheme", "Red").WithAction(fun _ -> setPalette 2)
                                        RadioMenuItem("colorscheme", "Green").WithAction(fun _ -> setPalette 3)
                                    ])
                            SubMenu
                                ("Iterations",
                                    [
                                        RadioMenuItem("iter", "150").WithCheck().WithAction(itchg)
                                        RadioMenuItem("iter", "250").WithAction(itchg)
                                        RadioMenuItem("iter", "500").WithAction(itchg)
                                        RadioMenuItem("iter", "1000").WithAction(itchg)
                                    ])
                            SubMenu
                                ("Fill mode",
                                    [
                                        RadioMenuItem("fillmode", "Line").WithAction(fun _ -> setFillMode linearFill)
                                        RadioMenuItem("fillmode", "Block").WithCheck().WithAction(fun _ -> setFillMode blockFill)
                                    ])
                        ])

            let copyf () =
                let maxit = (iterations tl br)
                let tlx, tly = tl
                let brx, bry = br
                clipboard.Text <- sprintf "<Mandel iter=\"%d\"><topleft><re>%.14e</re><im>%.14e</im></topleft><bottomright><re>%.14e</re><im>%.14e</im></bottomright></Mandel>" maxit tlx tly brx bry

            let pastef () =
                if clipboard.Text.StartsWith("<Mandel") then
                    let doc = new System.Xml.XmlDocument()
                    try
                        doc.LoadXml(clipboard.Text)
                        menuIterations <- int(doc.SelectSingleNode("/Mandel").Attributes.["iter"].Value)
                        tl <- (float(doc.SelectSingleNode("//topleft/re").InnerText), float(doc.SelectSingleNode("//topleft/im").InnerText))
                        br <- (float(doc.SelectSingleNode("//bottomright/re").InnerText), float(doc.SelectSingleNode("//bottomright/im").InnerText))
                        rect <- RectangleF.Empty
                        updateView()
                    with _ -> ()

            let e =
                SubMenu 
                    ("&Edit", 
                        [
                            ActionMenuItem("&Copy").WithAction(fun _ -> copyf()).WithShortcut(Keys.Control ||| Keys.C)
                            ActionMenuItem("&Paste").WithAction(fun _ -> pastef()).WithShortcut(Keys.Control ||| Keys.V)
                            ActionMenuItem("Copy &bitmap").WithAction(fun _ -> lock bitmap (fun _ -> clipboard.Image <- bitmap)).WithShortcut(Keys.Control ||| Keys.Shift ||| Keys.C)
                            ActionMenuItem("Zoom &In").WithAction(fun _ -> zoom 0.9 tl br).WithShortcut(Keys.Control ||| Keys.T)
                            ActionMenuItem("Zoom &Out").WithAction(fun _ -> zoom 1.25 tl br).WithShortcut(Keys.Control ||| Keys.W)
                        ])

            [f; c; e] |> List.iter (fun i -> m.Items.Add(i |> makeMenu))
            m

        clearOffScreen bitmap
        form.Menu <- setupMenu()
        draw.Paint.Add(fun arg -> paint arg.Graphics)
        draw.MouseDoubleClick.Add(click)
        draw.MouseMove.Add(mouseMove)
        form.Content <- draw
        form.SizeChanged.Add(fun _ -> resize())

        app.RunIteration()

        drawMandel()
        app.Run(form)
    with
    | ex -> printfn "%s" ex.Message

    0 // return an integer exit code
